module.exports = function() {
  var msPath = arguments[0];

  // return 'https://github.com/trufflesuite/trufflesuite.com/edit/master/src/' + msPath.dir;
  // return 'https://gitlab.com/jesoos/jesoos.gitlab.io/edit/master/src/' + msPath.dir;  // edit 로 하면 gitlab 로그인을 해야함.
  return 'https://gitlab.com/jesoos/jesoos.gitlab.io/tree/master/src/' + msPath.dir;
};
