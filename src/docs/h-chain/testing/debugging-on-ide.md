---
title: Truffle | Debugging on IDE
layout: docs.hbs
---
# Debugging on IDE

`IDE`의 `Built-in Debugger`(내장된 디버거)를 사용하여 Javascript로 작성된 `Dapp`(Test Code) 디버깅


## Prerequisites

[NPM Installation](https://www.npmjs.com/get-npm)


## Table of Contents

1. [Truffle Installation](#truffle-installation)
2. [Creating a MetaCoin project](#creating-a-metacoin-project)
3. [Creating a package.json file](#creating-a-package-json-file)
4. [Truffle Package Installation](#truffle-package-installation)
5. [The Whole Shell Script](#the-whole-shell-script)
6. [On IntelliJ(or WebStorm) IDE Ultimate Edition](#on-intellij-or-webstorm-ide-ultimate-edition)
7. [Using OpenZeppelin](#using-openzeppelin)
7. [Using MultiSigWallet](#using-multisigwallet)
8. [On Visual Studio Code](#on-visual-studio-code)


## Truffle Installation

1. Truffle을 npm을 사용하여 설치한다.

   ```shell
   npm install -g truffle
   ```


## Creating a MetaCoin project 

Truffle의 명령어를 실행하기 위해서는 Truffle project가 필요.
[Truffle Boxes](https://truffleframework.com/boxes)를 사용하여 [MetaCoin box](https://truffleframework.com/boxes/metacoin)를 Truffle project로 사용.

1. Create a new directory for your Truffle project:

   ```shell
   mkdir MetaCoin
   cd MetaCoin
   ```
1. Download ("unbox") the MetaCoin box:

   ```shell
   truffle unbox metacoin
   ```

<p class="alert alert-info">
<strong>Note</strong>: 보다 자세한 내용을 위해 Truffle 공식 홈페이지의 [Creating a project](https://truffleframework.com/docs/truffle/quickstart#creating-a-project) 참고.
</p>


## Creating a package.json file

1. npm의 package를 사용하기 위해 package.json파일을 생성:

   ```shell
   npm init --yes
   ```

1. package.json파일을 Commandline에서 수정하여 위해 json 패키지 설치:

   ```shell
   npm install -g json
   ```

### Adding Test Script

   <p class="alert alert-info">
   <strong>Note</strong>: OS별 `환경변수`의 차이로 각각 다른 Script 사용. Windows의 `Powershell`에서는 오류가 발생될 수 있으므로 `CMD`에서 수행 권장.
   </p>

1. on Mac:

   ```shell
   json -I -f package.json -e this.scripts.test='"node $NODE_DEBUG_OPTION ./node_modules/.bin/truffle test $FILES --network $NETWORK"'
   ```

1. on Windows:

   ```shell
   json -I -f package.json -e this.scripts.test='"node %NODE_DEBUG_OPTION% ./node_modules/truffle/build/cli.bundled.js test"'
   ```


<!-- on PowerShell
Quoting Error

```shell
undefined:3
this.scripts.test=node $NODE_DEBUG_OPTION ./node_modules/.bin/truffle test $FILES --network $NETWORK
                       ^^^^^^^^^^^^^^^^^^

SyntaxError: Unexpected identifier
```




[Error](#error-1)

powershell 에서
json -I -f package.json -e this.scripts.test='""node $NODE_DEBUG_OPTION ./node_modules/.bin/truffle test $FILES --network $NETWORK""'



node $NODE_DEBUG_OPTION ./node_modules/.bin/truffle test $FILES --network $NETWORK
입력

```shell
npm init
...
npm install truffle --save-dev
```

https://docs.npmjs.com/cli/init.html

# Error

## Error 1

### MetaCoin
```shell
mkdir myproject && cd myproject
truffle init
```

### Resolving naming conflicts on Windows

powershell

error 
자세하게 적을 것인지 결정하기
-->


## Truffle Package Installation
 
1. Npm을 통해 Truffle Package 설치:

   ```shell
   npm install truffle --save-dev
   ```
   <p class="alert alert-info">
   <strong>Note</strong>: IDE에서 디버깅하기 위해 필수로 필요.
   </p>


## The Whole Shell Script

1. The Whole shell script:

   ```shell
   npm install -g truffle
   mkdir MetaCoin
   cd MetaCoin
   truffle unbox metacoin
   npm init --yes
   npm install -g json
   json -I -f package.json -e this.scripts.test='"node %NODE_DEBUG_OPTION% ./node_modules/truffle/build/cli.bundled.js test"'
   npm install truffle --save-dev
   ```

1. Mac에서는 아래 명령어를 추가로 실행하거나 Shell Script를 변경해서 실행:

   ```shell
   json -I -f package.json -e this.scripts.test='"node $NODE_DEBUG_OPTION ./node_modules/.bin/truffle test $FILES --network $NETWORK"'
   ```

   <p class="alert alert-info">
   <strong>Note</strong>: [터미널에서 실행 결과 녹화 화면(on Mac)](https://asciinema.org/a/c0Wo5F4aGxEmuDxnsqJDF8tac)
   </p>


## On IntelliJ(or WebStorm) IDE Ultimate Edition

   <p class="alert alert-info">
   <strong>Note</strong>: [Debugging Solidity Smart Contracts Inside of the IntelliJ IDE](https://medium.com/80trill/debugging-solidity-smart-contracts-inside-of-the-intellij-ide-5b97d857143d) Tutorial을 기반으로 작성. This tutorial is for the `paid version` of IntelliJ or WebStorm.
   </p>

1. [Up-to-date installation of the IntelliJ (or WebStorm) IDE Ultimate Edition (Paid Version)](https://www.jetbrains.com/idea/)

1. Truffle Project Open:
   ![](.debugging-on-ide_images1/bdb7380c.png)
   
   `Or`
   
   * 해당 디렉토리로 이동 후:
   ```shell
   cd MetaCoin
   idea .
   ```

1. Inside of edit configurations, you will need to create a new run/debug configuration:
   ![](.debugging-on-ide_images1/831c1780.png)
   ![](.debugging-on-ide_images1/fc096993.png)
   ![](.debugging-on-ide_images1/05c2b654.png)
   ![](.debugging-on-ide_images1/053f9c3a.png)
   ![](.debugging-on-ide_images1/e2c3a8f8.png)

1. Run the debugger with the following breakpoints and examine how it works:
   ![](.debugging-on-ide_images1/9421e817.png)

1. Debugger Console output:
   ![](.debugging-on-ide_images1/b01976cf.png)

<!--
...
❯ 
```
npm init   
This utility will walk you through creating a package.json file.
It only covers the most common items, and tries to guess sensible defaults.

See `npm help json` for definitive documentation on these fields
and exactly what they do.

Use `npm install <pkg>` afterwards to install a package and
save it as a dependency in the package.json file.

Press ^C at any time to quit.
package name: (metacoin) 
version: (1.0.0) 
description: 
entry point: (truffle-config.js) truffle.js
test command: node $NODE_DEBUG_OPTION ./node_modules/.bin/truffle test $FILES --network $NETWORK
git repository: 
keywords: 
author: jesoos
license: (ISC) 
```
-->


## Using OpenZeppelin

1. [Git Installation](https://git-scm.com/download)

   <p class="alert alert-info">
   <strong>Note</strong>: npm install 시 사용됨.
   </p>

1. [Source Code](https://github.com/OpenZeppelin/openzeppelin-solidity) Clone or download:
   ![](.debugging-on-ide_images1/cea76cf6.png) 

1. File: `truffle-config.json` or `truffle.json` 내용 모두 주석처리

   <p class="alert alert-info">
   <strong>Note</strong>: a built-in personal blockchain을 사용하기 위함.
   </p>

   <p class="alert alert-info">
   <strong>Note</strong>:  `Ganache` 등의 Ethereum clients를 사용할 경우 networks의 development 설정하여 사용. See the [Configuration](https://truffleframework.com/docs/truffle/reference/configuration#networks) section for more information.
   </p>

1. `npm install` 실행으로 package 설치:

   ```shell
   npm install
   ```

   <p class="alert alert-info">
   <strong>Note</strong>: `gyp ERR`가 발생되어도 디버깅에 지장없음.
   </p>

1. Adding Test Script on a `./package.json` File:

    * on Mac
        ```javascript
        ...
          "scripts": {
            "test:debug": "node $NODE_DEBUG_OPTION ./node_modules/.bin/truffle test $FILES --network $NETWORK",
        ...
        ```

    * on Windows
        ```javascript
        ...
          "scripts": {
            "test:debug": "node %NODE_DEBUG_OPTION% ./node_modules/.bin/truffle test",
        ...
        ```

<!--
```shell
C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master>npm install
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\string-width-dc0aeb14\package.json'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\string-width-dc0aeb14\index.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\string-width-dc0aeb14\license'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\symbol-observable-a3fc4c19\es\index.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\string-width-dc0aeb14\readme.md'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\symbol-observable-a3fc4c19\es\ponyfill.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\jscodeshift-82a894a9\docs\fonts\OpenSans-Bold-webfont.eot'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\mocha-a02499e2\bin\_mocha'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\inquirer-a57a3e7f\lib\prompts\base.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\inquirer-a57a3e7f\lib\prompts\checkbox.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\inquirer-a57a3e7f\lib\prompts\confirm.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\inquirer-a57a3e7f\lib\prompts\editor.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\fs-extra-8565299f\lib\mkdirs\index.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\inquirer-a57a3e7f\lib\prompts\expand.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\fs-extra-8565299f\lib\mkdirs\mkdirs-sync.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\fs-extra-8565299f\lib\mkdirs\mkdirs.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\inquirer-a57a3e7f\lib\prompts\input.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\fs-extra-8565299f\lib\mkdirs\win32.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\inquirer-a57a3e7f\lib\prompts\list.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\inquirer-a57a3e7f\lib\prompts\password.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\inquirer-a57a3e7f\lib\prompts\rawlist.js'
npm WARN tar ENOENT: no such file or directory, lstat 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\fs-extra-8565299f\lib\move'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\solium-cd434319\test\lib\rules\array-declarations\fixed\ws-btw-op-clos.sol'
npm WARN tar ENOENT: no such file or directory, lstat 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\solium-cd434319\test\lib'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\handlebars-a211f3e8\dist\amd\handlebars\helpers\block-helper-missing.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\handlebars-a211f3e8\dist\amd\handlebars\helpers\each.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\handlebars-a211f3e8\dist\amd\handlebars\helpers\helper-missing.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\handlebars-a211f3e8\dist\amd\handlebars\helpers\if.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\handlebars-a211f3e8\dist\amd\handlebars\helpers\log.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\handlebars-a211f3e8\dist\amd\handlebars\helpers\lookup.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\handlebars-a211f3e8\dist\amd\handlebars\helpers\with.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\webpack-cli-d98e727c\lib\migrate\noEmitOnErrorsPlugin\__testfixtures__\.editorconfig'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\webpack-cli-d98e727c\lib\migrate\noEmitOnErrorsPlugin\__testfixtures__\noEmitOnErrorsPlugin-0.input.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\webpack-cli-d98e727c\lib\migrate\noEmitOnErrorsPlugin\__testfixtures__\noEmitOnErrorsPlugin-1.input.js'
npm WARN tar ENOENT: no such file or directory, open 'C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\.staging\webpack-cli-d98e727c\lib\migrate\noEmitOnErrorsPlugin\__testfixtures__\noEmitOnErrorsPlugin-2.input.js'
```


C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\scrypt>if not defined npm_config_node_gyp (node "C:\Users\roisun\AppData\Roaming\npm\node_modules\npm\node_modules\npm-lifecycle\node-gyp-bin\\..\..\node_modules\node-gyp\bin\node-gyp.js" rebuild )  else (node "C:\Users\roisun\AppData\Roaming\npm\node_modules\npm\node_modules\node-gyp\bin\node-gyp.js" rebuild )
gyp ERR! configure error
gyp ERR! stack Error: Can't find Python executable "python", you can set the PYTHON env variable.
gyp ERR! stack     at PythonFinder.failNoPython (C:\Users\roisun\AppData\Roaming\npm\node_modules\npm\node_modules\node-gyp\lib\configure.js:484:19)
gyp ERR! stack     at PythonFinder.<anonymous> (C:\Users\roisun\AppData\Roaming\npm\node_modules\npm\node_modules\node-gyp\lib\configure.js:509:16)
gyp ERR! stack     at C:\Users\roisun\AppData\Roaming\npm\node_modules\npm\node_modules\graceful-fs\polyfills.js:284:29
gyp ERR! stack     at FSReqWrap.oncomplete (fs.js:154:21)
gyp ERR! System Windows_NT 10.0.17134
gyp ERR! command "C:\\Program Files\\nodejs\\node.exe" "C:\\Users\\roisun\\AppData\\Roaming\\npm\\node_modules\\npm\\node_modules\\node-gyp\\bin\\node-gyp.js" "rebuild"
gyp ERR! cwd C:\Users\roisun\openzeppelin-solidity-master\openzeppelin-solidity-master\node_modules\scrypt
gyp ERR! node -v v10.13.0
gyp ERR! node-gyp -v v3.8.0
gyp ERR! not ok
npm WARN webpack-cli@2.1.5 requires a peer of webpack@^4.0.0 but none is installed. You must install peer dependencies yourself.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.4 (node_modules\fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.4: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})

npm ERR! code ELIFECYCLE
npm ERR! errno 1
npm ERR! scrypt@6.0.3 install: `node-gyp rebuild`
npm ERR! Exit status 1
npm ERR!
npm ERR! Failed at the scrypt@6.0.3 install script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     C:\Users\roisun\AppData\Roaming\npm-cache\_logs\2018-11-21T10_07_31_276Z-debug.log

위와 같은 에러가 발생이 되어도 디버깅에 문제는 없음
-->


## Using MultiSigWallet

1. [Source Code](https://github.com/gnosis/MultiSigWallet) Clone or download:
    ![](.debugging-on-ide_images1/7fd06a58.png) 

1. Npm의 truffle package를 최신 버전으로 재설치

   ```shell 
   npm install truffle@latest --save-dev
   ```
   <p class="alert alert-info">
   <strong>Note</strong>: 과거 버전인 경우 a built-in personal blockchain 사용이 안 될 수 있음.
   </p>

1. 나머지 과정은 [Using OpenZeppelin](#using-openzeppelin)과 동일


<!--
npm uninstall *
> 이걸로 모두 제거 되지 않았음

for package in `ls node_modules`; do npm uninstall $package; done;
https://stackoverflow.com/questions/19106284/how-do-you-uninstall-all-dependencies-listed-in-package-json-npm




npm ls -g --depth=0
-->


## On Visual Studio Code

1. [Up-to-date installation of the Visual Studio Code](https://code.visualstudio.com/)

1. Truffle Project Open:
   ![](.debugging-on-ide_images1/a40b86db.png)
   `Or`
   * 해당 디렉토리로 이동 후:
   ```shell
   cd MetaCoin
   code .
   ```

1. Debug (Ctrl+Shift+X) 버튼 클릭:
   ![](.debugging-on-ide_images1/e6994d61.png)

1. 톱니바퀴 모양 클릭(Configure or Fix `launch.json`) 후 Node.js 선택: 
   ![](.debugging-on-ide_images1/ec464943.png)
   ![](.debugging-on-ide_images1/056c3055.png)

1. File: launch.json에 아래 코드 추가:
   ```javascript
           {
               "name": "run tests",
               "type": "node",
               "request": "launch",
               "program": "${workspaceRoot}/node_modules/truffle/build/cli.bundled.js",
               "args": ["test"],
               "cwd": "${workspaceRoot}",
               "outFiles": [
                   "${workspaceRoot}/test/**/*"
               ]
           }
   ```   
   
1. Run the debugger with the following breakpoints and examine how it works And Debugger Console output:
   ![](.debugging-on-ide_images1/d1be7a58.png)
   ![](.debugging-on-ide_images1/3f988697.png)
