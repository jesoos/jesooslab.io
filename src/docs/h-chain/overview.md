---
title: H-Chain | Overview
layout: docs.hbs
---
<!--<img style="max-width: 160px;" src="/img/ganache-logo-dark.svg" alt="Ganache Logo" />-->

<!--<div class="text-center docs-badges">-->
  <!--[![Join the chat at https://gitter.im/consensys/truffle](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/consensys/truffle?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)-->
  <!--<a class="github-button" href="https://github.com/trufflesuite/ganache" data-icon="octicon-star" data-show-count="true" aria-label="Star trufflesuite/ganache on GitHub">Star</a>-->
  <!--<a class="github-button" href="https://github.com/trufflesuite/ganache/fork" data-icon="octicon-repo-forked" data-show-count="true" aria-label="Fork trufflesuite/ganache on GitHub">Fork</a>-->
<!--</div>-->

![](/img/ci_concept.png)

# H-Chain 3.0 Overview

한화 블록체인 플랫폼 고도화 사업 추진 내용
1. 사업명: 한화 블록체인 플랫폼 고도화
1. 사업기간: 2020년 1월 ~ 12월
1. 수행범위

   * 블록체인 플랫폼 고도화
   * 플랫폼 기반의 대내외 IT사업 참여를 위한 SDK확보 및 해당 기술 PoC
   * 블록체인 전문인력 양성을 위한 교육 과정 개발

## Goal(목적)

본 문서는 개발 된 Smart Contract 를 한화 Blockchain Platform 환경에 적용 및 활용하기 위한 방법을 설명한다.

## Scope(적용 범위)

- 일반적인 Ethereum Blockchain 환경에서 Truffle, Ganache 을 이용한 Smart Contract의 개발, 테스트 및 배포.
- 한화 Blockchain Platform 환경에서의 Smart Contract 개발, 테스트 및 배포.
- 한화 Blockchain Platform 환경에서의 Application 개발 및 테스트.

## Etc.(기타)

- Mac OS 환경을 기준으로 한다.
- Ethereum, Solidity, Truffle, Ganache 등에 대한 기본적인 지식을 가지고 있으며 관련 개발 환경이 구축되어 있다는 가정 하에 설명한다.
- NPM, Nodejs, Restful API, Postman, json 등에 대한 기본적인 지식을 가지고 있으며 관련 개발 환경이 구축되어 있다는 가정 하에 설명한다.


<!--
[Ganache](/ganache) is a personal blockchain for Ethereum development you can use to deploy contracts, develop your applications, and run tests.

It is available as both a desktop application as well as a command-line tool (formerly known as the TestRPC). Ganache is available for Windows, Mac, and Linux.
-->

## References

1. 이 사이트는 [Truffle Suite Website](https://github.com/trufflesuite/trufflesuite.com)에서 공개한 소스를 재구성하여 만든 웹 사이트이다.
1. [Honechain 따라하기_v0.9.pdf](https://sncsp.eagleoffice.co.kr/W/W00054/_layouts/15/WopiFrame2.aspx?sourcedoc=/W/W00054/D00001/개발%20가이드%20배포판/Honechain%20따라하기_v0.9.pdf&action=default&DefaultItemOpen=1)(박강훈 과장 작성,`비공개`) 기반으로 작성
