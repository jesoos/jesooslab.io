---
title: H-Chain | DApp Server Guide
layout: docs.hbs
---
# DApp Server Guide

내부에서 관리되는 내용을 포함하여 `비공개`됨.

pdf 문서로만 열람 가능.

<p class="alert alert-info">
<strong>Note</strong>: H-Chain 팀에 합류하시면 열람 가능합니다. jesoos@hanwha.com으로 연락주세요~!
</p>

## References

1. [HoneChain Server 개발환경 Guide_Ver1.0.pdf](https://sncsp.eagleoffice.co.kr/W/W00054/_layouts/15/WopiFrame2.aspx?sourcedoc=/W/W00054/D00001/개발%20가이드%20배포판/HoneChain%20Server%20개발환경%20Guide_Ver1.0.pdf&action=default&DefaultItemOpen=1)(내부문서,`비공개`) 기반으로 작성
