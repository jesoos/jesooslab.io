---
title: H-Chain | Git Terminal Guide
layout: docs.hbs
---
# Git Terminal Guide

내부에서 관리되는 내용을 포함하여 `비공개`됨.

pdf 문서로만 열람 가능.

<p class="alert alert-info">
<strong>Note</strong>: H-Chain 팀에 합류하시면 열람 가능합니다. jesoos@hanwha.com으로 연락주세요~!
</p>

## References

1. [Git Terminal Guide_1.0.pdf](https://sncsp.eagleoffice.co.kr/W/W00054/_layouts/15/WopiFrame.aspx?sourcedoc=/W/W00054/D00001/%EA%B0%9C%EB%B0%9C%20%EA%B0%80%EC%9D%B4%EB%93%9C%20%EB%B0%B0%ED%8F%AC%ED%8C%90/Git_Terminal_Guide.pdf&action=default&DefaultItemOpen=1)(내부문서,`비공개`) 기반으로 작성
