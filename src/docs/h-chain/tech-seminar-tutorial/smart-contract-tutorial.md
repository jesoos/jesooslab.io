---
title: H-Chain | Smart Contract Tutorial
layout: docs.hbs
---
# Smart Contract
스마트컨트랙트는 Nick Szabo가 1994년 처음 제안한 개념이다. 
기존 계약서는 문서로 작성이 되어있어서 계약을 시행하려면 실제 사람이 계약서대로 수행을 하는 시간이 걸리고 예기치 못한 일들이 생기는데에 비해, 스마트컨트랙트의 개념으로 계약을 시행하면 컴퓨터 명령어로 계약을 작성하기 때문에 조건이 충족되는 즉시 계약을 시행할 장점이 있다.
![](.smart-contract-tutorial_images/smartcontract.png)

# Solidity
Solidity는 스마트 컨트랙트를 구현하기 위한 컨트랙트 기반의 고급 프로그래밍 언어로 `.sol`의 확장자를 가진 파일이다. Solidity는 C++, Python, 그리고 JavaScript의 영향을 받아 만들어졌으며 Ethereum Virtual Machine(EVM)에서 구동되도록 설계되었다.

  ```
  HelloWorld.sol
  ```

# 기본 자료형

  ```
  uint8, uint16, uint24, ... uint256
  >> uint balance = 100;
  ```  
   * unsigned integer 로 uint는 uint256와 같다.
  ```
  address owner = 0x0cE446255506E92DF41614C46F1d6df9Cc969183
  ```
   * 20바이트의 16진수로 이더리움 Account의 주소를 나타낸다.
  ```
  mapping (Data Type => Data Type)
  >> mapping (address => uint) public accountBalance; //유저의 계좌 잔액을 보유 하는 uint 지정가능  
  >> mapping (uint => string) userIdToName; //key는 uint 이고 값은 string 이다.
  ``` 
   * 타 언어의 map과 유사하게 key value로 이루어진 자료구조
  
# 알아둬야할 Solidity 특징
 
### 1. 변수의 Data 저장 구조 
 
 * 함수 매개 변수(리턴 매개 변수 포함)의 기본 위치는 `memory`이고, local 변수의 기본 위치는 `storage`며 state 변수의 경우 강제로 `storage`에 저장된다.
 
   - state 변수 : contract 최상위단에 선언된 변수(`storage`에 저장)
   - local 변수 : 함수 아래에 선언된 변수(`storage`에 저장, 키워드로 `memory` 저장 가능)
  
### 2. 함수 제어자

 * 함수 접근 제어
 
   - `public` : 외/내부 접근 제한이 없고 public으로 선언된 변수는 자동으로 getter가 생성된다.
   - `private` : 외부에서 접근 불가능하다.
   - `internal` : 외부에서 접근 불가능하지만 상속 받은 contract에서는 접근 가능
   - `external` : contract 내에서는 호출 불가능 하고 외부에서만 호출 가능
 
 * 함수 상태 제어
   ```
   function say() public view returns (string) {
     return greeting;
   }
   
   function _multiply(uint a, uint b) private pure returns (uint) {
     return a * b;
   }
   ``` 
   - `view` : 함수가 데이터를 보기만 하고 데이터는 변경하지 않음을 보장한다.
   - `pure` : 어떤 데이터 접근하지 않는다는 것을 보장한다. 함수의 인자값을 사용한다.
   
 * 특수 전역 변수
   - `block` : 블록에 대한 정보를 접근할 수 있다. (블록 채굴자 주소, 난이도, 블록 번호 ...)
   - `msg` : 계약을 생성한 계정의 정보를 담고 있다. (계약 발신자, 송금하는 금액, 데이터 ...)
   - `tx` : 해당 tx에 대한 정보를 담고 있다. (tx의 가스비용, tx의 발신자 ...)
 
### 3. 기타
   - `payable` : 계약 계정이 외부에서 ETH를 송금 받을 수 있게함
   - `modifier` : 함수를 실행하기 전에 동작 조건을 확인하고 함수 실행을 제어하는 수식자
   
# 실습 - HelloWorld 코딩해보기
   ```solidity
   pragma solidity ^0.4.24;
   
   contract HelloWorld {
       string public greeting;
       
       function HelloWorld(string _greeting) public {
           greeting = _greeting;
       }
       function setGreeting(string _greeting) public {
           greeting = _greeting;
       }
       function say() public view returns(string) {
           return greeting;
       }
   }
   ```
   * 주요 코드 설명
   
      - `pragma solidity ^0.4.24` : 컴파일러의 버전을 지정하는 명령이다. solidity 0.4.24 버전을 기반으로 작성되었다는 것을 뜻한다.
      - `contract HelloWorld { ... }` : contract로 계약을 선언한다.
      - `public` : 계약에 접근할 수 있는 사용자라면 누구나 열람 가능하며 state 변수의 경우 컴파일러가 자동으로 getter를 생성해 준다.
      - `returns` : solidity의 경우 함수 선언 시 return 값의 type을 정의해준다.



# Remix
Solidity를 프로그래밍 할 수 있는 개발 툴으로 복잡한 설치가 필요없고 웹 브라우저에서 항상 최신 버전을 사용할 수 있다.
([Remix 바로가기](https://remix.ethereum.org))

![](.smart-contract-tutorial_images/remixEx.png)
    
    Remix는 3가지 영역으로 구성되어있다.
    가장 왼쪽이 소스 코드 브라우저이고, 가운데가 소스코드, 에디터 가장 우측이 컨트랙트의 컴파일, 배포, 디버깅 등의 분석 기능을 가지고 있다.  

    
   ### 1. 코드작성
   ![](.smart-contract-tutorial_images/HelloWorld.png)
   
    사전에 작성했던 코드를 Remix에 Copy & Paste 한다

   
   ### 2. 컴파일
   ![](.smart-contract-tutorial_images/컴파일.png)
      
    Compile version을 0.4.24으로 선택한 후 컴파일 버튼을 누른다.

   
   ### 3. VM 설정
   ![](.smart-contract-tutorial_images/remix_jvm.png)
    
   - Javascript VM : geth 노드 연결 없이 모든 개발이 로컬 컴퓨터의 Remix의 메모리에서 처리됩니다.
   - Injected Web3 : Mist나 Metamask 같이 Mist와 유사한 공급자에 의해 제공되는 실행 환경을 이용합니다.
   - Web3 provider : 로컬 컴퓨터에서 작동되는 geth 노드에 연결하여 수행됩니다. 이 환경에서는 트랜잭션이 네트워크를 통해 전달될 수 있습니다.
            
    현재 따로 Geth를 구동하고 있지 않으므로 JavaScript VM 환경을 선택한다.
      (Local에서 Geth를 실행했을 Web3 Provider 으로 연결 가능하다.)
      

   
   ### 4. 배포
   ![](.smart-contract-tutorial_images/remix_hellworld.png)
   
    Constructor 의 인자값을 설정하고("hello world") Deploy 버튼을 눌러준다.
   
   ![](.smart-contract-tutorial_images/check_deploy.png)
   
    다음과 같이 블록체인 네트워크에 배포가 되었음을 알 수 있다.

   
   ### 5. 호출
   ![](.smart-contract-tutorial_images/call.png)
   
    배포된 HelloWorld 계약을 호출해보는 과정이다. say 함수 버튼을 클릭한다.
   
   ![](.smart-contract-tutorial_images/check_call.png)
   
    다음과 같이 output이 hello world로 출력되는 것을 확인할 수 있다.

    


