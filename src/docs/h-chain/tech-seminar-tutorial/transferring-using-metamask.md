---
title: H-Chain | Transferring Using MetaMask
layout: docs.hbs
---
# Transferring Using MetaMask

메타마스크 활용한 암호화폐 전송

## Installation MetaMask

`MetaMask`는 이더리움을 기반으로 만든 `지갑`. 이더리움을 기반으로 한 코인들만 저장할 수 있음.

### PC

https://coinsutra.com/use-metamask-wallet/

### Android

https://www.cryptokosh.com/tutorial-use-metamask-wallet-on-android-ios/

## Connecting To An Ethereum Node

이더리움 노드에 접속하기

### Private Network

`실습용 블록체인 docker 설명추가`

1. RPC URL: https://jesoos-ganache-cli.herokuapp.com(`실습용 블록체인`)
   * `RPC` : 원격 프로시저 호출(영어: remote procedure call, 리모트 프로시저 콜, RPC)은 별도의 원격 제어를 위한 코딩 없이 다른 주소 공간에서 함수나 프로시저를 실행할 수 있게하는 프로세스 간 통신 기술이다. 다시 말해, 원격 프로시저 호출을 이용하면 프로그래머는 `함수`가 실행 프로그램에 `로컬` 위치에 있든 `원격` 위치에 있든 `동일한 코드`를 이용할 수 있다.
   * JSON-RPC : JSON으로 인코딩된 원격 프로시저 호출이다.
      - 이더리움에서는 JSON-RPC를 사용함

1. Accounts(계정) 및 Private Keys(개인키):
   * `실습용 블록체인`전용으로 사용가능한 계정과 개인키

   ```shell
   Available Accounts
   ==================
   (0) 0x4efd4ab8293b3b8179d9dbf9ade1ae00d83eb314 (~500 ETH)
   (1) 0xaffe3d426b8517b17b54290c882f0827f65ec187 (~500 ETH)
   (2) 0x6824385051e746ef4c1c6b4ae7e56a381a95d54a (~500 ETH)
   (3) 0x338da826cf7a3c9a68f00a9e922eeed5ca1e8211 (~500 ETH)
   (4) 0x8dcc6e12c380a80329e6108ad9b04b78e561d65a (~500 ETH)
   (5) 0xcee6938394b0fa55c45d08ad588cd84af89a14df (~500 ETH)
   (6) 0x70ab1d88d6980b0d192f3521f8862ac4dca68567 (~500 ETH)
   (7) 0xe20547f96055fbd2c3a2263e71cd4adb74b69349 (~500 ETH)
   (8) 0x79bef342f6bc8e0b34bcf51e55bf73611aeeb2c4 (~500 ETH)
   (9) 0xa24982b1e3c6be086465971c9e7e7e8ad44fdf48 (~500 ETH)
   (10) 0x15693eefda627d936d2354cbdccc6064ac910664 (~500 ETH)
   (11) 0xcd8b2732fb0b432facd73dfa1ceb1979be1b1a94 (~500 ETH)
   (12) 0x342e2f4a71cbdc68e6bd583523315cf4eb4b94cb (~500 ETH)
   (13) 0x14c9afe879f61ca1c2bb86c124a2c15dd011497f (~500 ETH)
   (14) 0x34864b6ad93e64170d4852a22bd2c4073c069ffb (~500 ETH)
   (15) 0xae99e667f5a77d67326bba2a912ec442cc193048 (~500 ETH)
   (16) 0x424fc7d50aaf9de5a180c69331dd82a252bfb201 (~500 ETH)
   (17) 0xb03444c7543b9c23ea44ffcfd16d1fb81b9d9163 (~500 ETH)
   (18) 0xf0da8c5f37f21b1f8d993a240d3e6a1da0b5d85e (~500 ETH)
   (19) 0x715adc2d8d1b57cfcad0d988d6c7df0a340edbfa (~500 ETH)
   (20) 0x10646828c910201a04be515ec9fb03e218c86b25 (~500 ETH)
   (21) 0x9039b3fc76e635a55efdaf2640bba59b6ed1a0fc (~500 ETH)
   (22) 0x98896691ff8b64fd621456d393c0b4905ce6c388 (~500 ETH)
   (23) 0x760b53635e48cc4cf49d79477067e315e81b54a2 (~500 ETH)
   (24) 0x0f8d49159ae82ae2928558907c5d541ab9a8f567 (~500 ETH)
   (25) 0xcf53e34d608a29e6a387cad9d3b2259f2805b65a (~500 ETH)
   (26) 0x8a5aa9246b246542d6a6b188f3d2e756a7c840a0 (~500 ETH)
   (27) 0xea08a526c51e69155061360213d8d94a986e1a32 (~500 ETH)
   (28) 0xe84c420b418560d769bc9868d213b5ed3b30f7eb (~500 ETH)
   (29) 0xbe1209dcda78582941a28d00e0c712883d8f46e3 (~500 ETH)

   Private Keys
   ==================
   (0) 0x3f22cc3e1757c4a69de7e249c99e4217d4a0017157247a863cc7fb61e5a16ec8
   (1) 0x8cfce94fa87c2e937d2c901b6193802900e3a9b15bdfe9aaeb1cbb9e9b46d485
   (2) 0x3b9f3f9087260beb3c81a7fa105e0c0714cf30964d726a25cafb883bf3589c2e
   (3) 0x14886ce9c977b5b181cfc01ded5a9f4753f4340623bed643cd7ea89a777e36e7
   (4) 0xc8486cd25cb5278481eef49cb689905511f0ebd77101c3f87bb25c152d8a248a
   (5) 0x6ba903be3121fdacb40b677f8f5c0a400e9ff06d0632f4f9c0fcaf5ef4cf989e
   (6) 0x3141fae5ba12e4a00260f9aec8fafa0d60e0339a4d68145c4950eaab67e55e79
   (7) 0xcb3a94619a4ced15e0efea940e74c8b5c45b4af7dddc38ad50a0b474cc633ed1
   (8) 0xa3b3c325cb77497c5f6ac65a169aa9bdbdc1387d9367f5ab4d83c76b18ed2651
   (9) 0x59b034e79634893393a58ed2be861c459c200be8622a04c267f90ad7a44607ca
   (10) 0xf9083836feab048e3a55e0acf24ea9125d574e3ffde8268f397ab1f7c0d6d555
   (11) 0xe3bfad9b21acaa31f5892b20ef8986a4a5e3ece0a61c335c6e38ae9d0cf3fe01
   (12) 0x2a4359776b08611b55864297f4212fc0646b208f7c32d65551a29f173d8f4417
   (13) 0x0523b786a9b060280d2f4ffbc10fadb108c762bbea26dbef0c5c6d853db84889
   (14) 0x6e6ffa0054f10e561032fbcec6947c51c2beb449dddd166471f915b24c02e85b
   (15) 0xd8b857638975ad309c146dbc220cd04d39d5b4293174e12df99f1ad3a1f6d7ae
   (16) 0x4b153473c116533b3ab886504bff168cc74a91df9c6195d7c46d93d49c35b890
   (17) 0xd7f2b436c5dd8b436c31351d0ebef010ceaa7e97d5b8e4dcbb74ffd7dad47872
   (18) 0xbed64c35822a78b35037d1fbe210cc65dfccfe207a1ded477ebfa89b4a4091a4
   (19) 0x413f604ac333f1aa031b36a7974151c12ac050c00ae946eb54a872c21be0928f
   (20) 0x5a81c3aebd4ea9ae38f751c260e81fcbd7be94ecc1ed2eb7b6f9ba1c110f580e
   (21) 0xe75bbe7ee7ad04bfb52943fdfcceabee7d2b89d2e7f7eaca97382a66c91d0410
   (22) 0x9b187d4e07645b949c86d87becda4f4cc0ef7afc38c95a3eb80bc4692ab6d026
   (23) 0x87784f0d41777a038bdb73ba671938c469551afdf8583064c046011e50cb4cca
   (24) 0x4e126a94d6fd69e627a0c245d9de82544144271a29c64a5886bf40a3aeff2066
   (25) 0x2919e3e47af4bbddacad71a2db565d47f9ca42c4d23a924c3a1c977815c17ecf
   (26) 0xf1eb77d615782df0a3ccb11f9aa04f3b473ef8d48a62b73ad4b9611b2fa2d12c
   (27) 0x98e98788499a5853f241af78e1c0e09faf93735a08ec4240b815715e01bfefb9
   (28) 0xa446cf8aee588483ece581c86a491a653515e7149d8d4dcce530dde5681dfe37
   (29) 0xd6129bdd3065cb6670eabf8b3b7e434f5573fb7abcf20c30968a547eb6abe2d4

   HD Wallet
   ==================
   Mnemonic:      johnny mnemonic
   Base HD Path:  m/44'/60'/0'/0/{account_index}

   Gas Price
   ==================
   20000000000

   Gas Limit
   ==================
   1000000000

   Listening on 0.0.0.0:8545
   ```

1. `Private Keys(개인키) 중 하나를 복사한다.`

1. 오른쪽 상단의 아래와 같은 `동그란 버튼` 클릭:

   ![](.transferring-using-metamask_images1/2d0b7a0f.png)

1. `계정 가져오기` 클릭:

   ![](.transferring-using-metamask_images1/9ecb7fed.png)

1. 개인키 붙여넣기 후 `가져오기` 클릭:

   ![](.transferring-using-metamask_images1/c01aa44d.png)

1. 가져온 계정의 주소 확인: [Private Network](#private-network)
<p class="alert alert-info">
<strong>Note</strong>: 결국, 개인키(Private Key)만 있으면 계정주소(EOA)를 생성할 수 있음:<br />
`개인키` 관리가 무엇보다 중요.<br />
</p>

   * `EOA`(Externally Owned Accounts: 외부 소유 계정) : 외부적으로 관리되는 계정
       - 이더잔액을 가지고 있음
       - 트랜잭션을 전송할 수 있음(이더 거래 혹은 스마트 코드 작동)
       - 개인키(Private Key)로 관리됨
       - 관련 코드를 가지고 있지 않음

   ![](.transferring-using-metamask_images1/0e66620a.png)

1. `네트워크 드롭다운(아래 방향 화살표)` 클릭 후`사용자 정의 RPC` 클릭:

   ![](.transferring-using-metamask_images1/3989618a.png)

1. 스크롤을 조금 내린 다음 RPC URL 입력 후 저장 후 프라이빗 네트워크로 변경된 것을 확인 후 X 표시 클릭:

   ![](.transferring-using-metamask_images1/1bf8a22e.png)
   ![](.transferring-using-metamask_images1/0380dffd.png)

### Transferring ETH

ETH(Ether, 이더리움의 암호화폐)를 전송한다.

1. `전송` 클릭:

   ![](.transferring-using-metamask_images1/81a947a5.png)

1. 받는이의 `받는 주소` 칸 클릭하여 입력 or 선택:
   ![](.transferring-using-metamask_images1/91f95fdf.png)

1. 전송할 만큼의 `수량` 입력 후 다음 클릭:

   가스 설명 추가

   ![](.transferring-using-metamask_images1/f489ad2f.png)

1. 전송할 Total 을 확인 후 승인 클릭:
   ![](.transferring-using-metamask_images1/db6f34be.png)

1. 히스토리를 확인(전송된 이더를 클릭하면 상세내역도 확인가능):

   ![](.transferring-using-metamask_images1/0a5fead3.png)
   ![](.transferring-using-metamask_images1/875eb3bf.png)

## Smart Contract로 Token 발행 및 전송

Smart Contract 를 사용하여 Token 발행하고 전송을 해본다.

### REST API

`REST`(Representational State Transfer)는 월드 와이드 웹과 같은 분산 하이퍼미디어 시스템을 위한 소프트웨어 아키텍처의 한 형식이다. 이 용어는 로이 필딩(Roy Fielding)의 2000년 박사학위 논문에서 소개되었다. 필딩은 HTTP의 주요 저자 중 한 사람이다. 이 개념은 네트워킹 문화에 널리 퍼졌다.

간단한 의미로는, 웹 상의 자료를 `HTTP`위에서 SOAP이나 쿠키를 통한 세션 트랙킹 같은 별도의 전송 계층 없이 전송하기 위한 아주 간단한 인터페이스를 말한다.

### Interaction with blockchain

1. Ethereum에서 지원하는 방식:

  ![](.ico-exercise-using-self-token_images1/dc32e7c7.png)

## Token 발행

1. 토큰을 발행하기에 앞서 정의를 해야 하는 항목

   * INITIAL_SUPPLY 초기 수량
   * 토큰 이름(name)
   * 토큰 기호(symbol)
   * 소수점 정확도(decimal)

1. Browser 기반의 Solidity 개발 토구인 Remix:

   * https://remix.ethereum.org
     - (Remote Environment와 연결이 정상적으로 진행되지 않을 경우 `http://`로 변경 시도)

1. Remix에서 아래의 코드를 입력:

   ![](.ico-exercise-using-self-token_images1/d728e396.png)

   ![](.ico-exercise-using-self-token_images1/498b99e9.png)

   `File Name`:SimpleToken.sol 입력
   * ERC20 Token :
      * 토큰에 대한 표준을 제안하고자 만든 것이 ERC20.
      * 이더리움 플랫폼에서 표준을 제안하기 위해 개발자는 EIP (Ethereum Improvement Proposal 이더리움 개선 제안서)를 제출.
      * 이것이 승인을 받게되면 ERC가 되는 것. ERC20는 Ethereum Request for Comments의 줄임말로 이더리움 제안서에 따른 답변. 20은 제안서를 구분하기 위해 넣어진 숫자일 뿐임.

   ```javascript
   pragma solidity ^0.5.7;
   
   import "https://github.com/OpenZeppelin/openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
   import "https://github.com/OpenZeppelin/openzeppelin-solidity/contracts/token/ERC20/ERC20Detailed.sol";
   
   /**
    * @title SimpleToken
    * @dev Very simple ERC20 Token example, where all tokens are pre-assigned to the creator.
    * Note they can later distribute these tokens as they wish using `transfer` and other
    * `ERC20` functions.
    */
   contract SimpleToken is ERC20, ERC20Detailed {
   
     uint8 public constant DECIMALS = 18;
     uint256 public constant INITIAL_SUPPLY = 10000 * (10 ** uint256(DECIMALS));
   
     /**
      * @dev Constructor that gives msg.sender all of existing tokens.
      */
     constructor() public ERC20Detailed("HanwhaToken", "HWT", 18) {
       _mint(msg.sender, INITIAL_SUPPLY);
     }
   }
   ```

### Metamask에 토큰 추가

1. 토큰 주소 복사:

   * 미리 배포해 둔 토큰 주소 : 토큰 소유자(`0x4EFD4Ab8293b3B8179d9DBF9Ade1Ae00D83EB314`)
      1. HanwhaToken(HWT) : `0x06f7a5f35fd2cc2d53df0cad418f675a1e12db37`
      1. SimpleToken(SIM) : `0xf83647064a880f5d32cd4f24635c69de9beeeba7`

1. 메뉴 클릭 후 토큰 추가 클릭:

   ![](.ico-exercise-using-self-token_images1/fc1d153b.png)
   ![](.ico-exercise-using-self-token_images1/711ba013.png)

1. 사용자 정의 토큰 탭 선택 후 토큰 주소 붙여넣기:
   * 토큰 주소를 붙여넣으면 토큰 기호와 소수점 정확도가 자동으로 입력됨

   ![](.ico-exercise-using-self-token_images1/78ecadba.png)

1. 다음 버튼을 클릭하면 다음과 같은 화면이 보임. 토큰 추가 버튼 클릭:
   * 토큰을 구매하지 않거나 보상토큰으로 지급받지 않은 경우 최초는 0
   * 추가된 토큰에 잔액이 있는 경우 다른 계정으로 전송도 가능함

   ![](.ico-exercise-using-self-token_images1/9100d189.png)


## References


1. https://www.cryptokosh.com/tutorial-use-metamask-wallet-on-android-ios/
1. https://ko.wikipedia.org/wiki/원격_프로시저_호출
1. https://medium.com/yejipark/이더리움-계정-트랜잭션-가스-그리고-블록-가스-한도-89c5428078e6
