---
title: H-Chain | ICO Exercise Using Self Token
layout: docs.hbs
---
# ICO Exercise Using Self Token

나만의 암호화폐로 ICO 실습(Crowd Sale)

## API Server

1. H-Chain Architecture

   ![](.ico-exercise-using-self-token_images1/c9b7c9ca.png)

1. 인터페이스 (API-Server 구성)

   ![](.ico-exercise-using-self-token_images1/2d76217c.png)

### REST API

`REST`(Representational State Transfer)는 월드 와이드 웹과 같은 분산 하이퍼미디어 시스템을 위한 소프트웨어 아키텍처의 한 형식이다. 이 용어는 로이 필딩(Roy Fielding)의 2000년 박사학위 논문에서 소개되었다. 필딩은 HTTP의 주요 저자 중 한 사람이다. 이 개념은 네트워킹 문화에 널리 퍼졌다.

간단한 의미로는, 웹 상의 자료를 `HTTP`위에서 SOAP이나 쿠키를 통한 세션 트랙킹 같은 별도의 전송 계층 없이 전송하기 위한 아주 간단한 인터페이스를 말한다.

### Interaction with blockchain

1. Ethereum에서 지원하는 방식:

  ![](.ico-exercise-using-self-token_images1/dc32e7c7.png)
  <p class="alert alert-info">
  <strong>Note</strong>: `API Server`는 위와같은 작업을 대신 수행하여 Ethereum의 지식이 부족한 상황에서도 블록체인과 상호작용을 할 수 있도록 도와줌.<br />
  </p>

   * API Server Source Code 등은 내부에서 관리되는 내용을 포함하여 `비공개`됨.

   <p class="alert alert-info">
   <strong>Note</strong>: H-Chain 팀에 합류하시면 열람 가능합니다. jesoos@hanwha.com으로 연락주세요~!
   </p>

1. Postman을 통한 API Server와 Interaction 시연

## Token 발행

1. 토큰을 발행하기에 앞서 정의를 해야 하는 항목

   * INITIAL_SUPPLY 초기 수량
   * 토큰 이름(name)
   * 토큰 기호(symbol)
   * 소수점 정확도(decimal)

1. Browser 기반의 Solidity 개발 토구인 Remix:

   * https://remix.ethereum.org
     - (Remote Environment와 연결이 정상적으로 진행되지 않을 경우 `http://`로 변경 시도)

1. Remix에서 아래의 코드를 입력:

   ![](.ico-exercise-using-self-token_images1/d728e396.png)

   ![](.ico-exercise-using-self-token_images1/498b99e9.png)

   `File Name`:SimpleToken.sol 입력
   * ERC20 Token :
      * 토큰에 대한 표준을 제안하고자 만든 것이 ERC20.
      * 이더리움 플랫폼에서 표준을 제안하기 위해 개발자는 EIP (Ethereum Improvement Proposal 이더리움 개선 제안서)를 제출.
      * 이것이 승인을 받게되면 ERC가 되는 것. ERC20는 Ethereum Request for Comments의 줄임말로 이더리움 제안서에 따른 답변. 20은 제안서를 구분하기 위해 넣어진 숫자일 뿐임.

   ```javascript
   pragma solidity ^0.5.7;
   
   import "https://github.com/OpenZeppelin/openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
   import "https://github.com/OpenZeppelin/openzeppelin-solidity/contracts/token/ERC20/ERC20Detailed.sol";
   
   /**
    * @title SimpleToken
    * @dev Very simple ERC20 Token example, where all tokens are pre-assigned to the creator.
    * Note they can later distribute these tokens as they wish using `transfer` and other
    * `ERC20` functions.
    */
   contract SimpleToken is ERC20, ERC20Detailed {
   
     uint8 public constant DECIMALS = 18;
     uint256 public constant INITIAL_SUPPLY = 10000 * (10 ** uint256(DECIMALS));
   
     /**
      * @dev Constructor that gives msg.sender all of existing tokens.
      */
     constructor() public ERC20Detailed("HanwhaToken", "HWT", 18) {
       _mint(msg.sender, INITIAL_SUPPLY);
     }
   }
   ```

1. Select new compile version: `0.5.7+commit.6da8b019`
   * Compile Tab

   ![](.ico-exercise-using-self-token_images1/dbae5786.png)
   ![](.ico-exercise-using-self-token_images1/c1f92082.png)

1. Select Environment: `https://jesoos-ganache-cli.herokuapp.com`
   * Run Tab

   ![](.ico-exercise-using-self-token_images1/fb0a1572.png)
   ![](.ico-exercise-using-self-token_images1/87f8d355.png)
   ![](.ico-exercise-using-self-token_images1/50d2a721.png)

1. Deploy 버튼을 클릭하여 토큰 발행:

   ![](.ico-exercise-using-self-token_images1/b53f870f.png)
   ![](.ico-exercise-using-self-token_images1/6851b741.png)

## Crowdsale Deploy

1. Remix에서 아래의 코드를 입력:

   ![](.ico-exercise-using-self-token_images1/d728e396.png)

   ![](.ico-exercise-using-self-token_images1/498b99e9.png)

   `File Name`:Crowdsale.sol 입력

   ```javascript
   pragma solidity ^0.5.7;
   
   import "https://github.com/OpenZeppelin/openzeppelin-solidity/contracts/crowdsale/Crowdsale.sol";
   
   contract CrowdsaleMock is Crowdsale {
       constructor (uint256 rate, address payable wallet, IERC20 token) public Crowdsale(rate, wallet, token) {
           // solhint-disable-previous-line no-empty-blocks
       }
   }
   ```

1. 배포된 토큰 주소 복사:

   ![](.ico-exercise-using-self-token_images1/64bada53.png)

   * 미리 배포해 둔 토큰 주소 : `0x06f7a5f35fd2cc2d53df0cad418f675a1e12db37`

1. Deploy 버튼 오른쪽의 아래방향 화살표 버튼 클릭:

   ![](.ico-exercise-using-self-token_images1/0b8f1dfa.png)
   ![](.ico-exercise-using-self-token_images1/fe905087.png)

   * rate: 1
   * wallet: `0x6824385051e746ef4c1c6b4ae7e56a381a95d54a` (모금 받는 계정, ICO를 한 주체)
      - 여기에 이더가 쌓이게 됨
   * token: 복사한 토큰 주소 붙여넣기

1. parameter 입력 후 transact 버튼 클릭하여 Deploy:

### 토큰 계정에서 Crowdsale 계정으로 토큰 전송

ICO에 참여한 계정에게 지급될 토큰의 총량을 Crowdsale Contract에서 관리

1. 토큰의 총공급량(totalSupply) 복사:

   ![](.ico-exercise-using-self-token_images1/54a64896.png)

1. transfer 함수를 실행하여 전송:

   ![](.ico-exercise-using-self-token_images1/574ae0c9.png)

## ICO 참여

ICO에 참여하여 토큰을 지급받는다.

### Metamask에 토큰 추가

ICO 참여로 보상받는 토큰을 추가한다.

1. 토큰 주소 복사:

   ![](.ico-exercise-using-self-token_images1/fc769887.png)

   * 미리 배포해 둔 토큰 주소 : `0x06f7a5f35fd2cc2d53df0cad418f675a1e12db37`

1. 메뉴 클릭 후 토큰 추가 클릭:

   ![](.ico-exercise-using-self-token_images1/fc1d153b.png)
   ![](.ico-exercise-using-self-token_images1/711ba013.png)

1. 사용자 정의 토큰 탭 선택 후 토큰 주소 붙여넣기:
   * 토큰 주소를 붙여넣으면 토큰 기호와 소수점 정확도가 자동으로 입력됨

   ![](.ico-exercise-using-self-token_images1/78ecadba.png)

1. 다음 버튼을 클릭하면 다음과 같은 화면이 보임. 토큰 추가 버튼 클릭:
   * 토큰을 구매하지 않거나 보상토큰으로 지급받지 않은 경우 최초는 0
   * 추가된 토큰에 잔액이 있는 경우 다른 계정으로 전송도 가능함

   ![](.ico-exercise-using-self-token_images1/9100d189.png)

### ETH(이더) 보내기

1. Remix에서 CrowdsaleMock Contract 주소 복사: (복사 버튼 클릭)

   ![](.ico-exercise-using-self-token_images1/4f608e47.png)

   * 미리 배포해 둔 Crowsale 주소 : `0xde5a73e236e4f4085a7f9ec7834d97c507b2d69c`

1. MetaMask에서 전송 클릭:

   ![](.ico-exercise-using-self-token_images1/0f26e1ce.png)

1. Contract 주소 붙여넣기 및 수량 입력 후 다음버튼 클릭:

   ![](.ico-exercise-using-self-token_images1/f32161d9.png)

1. GAS FEE 의 EDIT 클릭 후 가스 한도만 변경: `1000000000` 로 변경
   * 가스 한도를 높게 설정한 이유는 Crowdsale의 함수가 수행되면서 많은 양의 가스를 소비하기 때문에 한도가 작게 되면 out of gas 오류가 발생됨.

   ![](.ico-exercise-using-self-token_images1/6d46e7e8.png)
   ![](.ico-exercise-using-self-token_images1/4f0f607f.png)

1. 이후 승인 버튼 클릭:

   ![](.ico-exercise-using-self-token_images1/af4af8a5.png)

1. 히스토리에서 확인:
   ![](.ico-exercise-using-self-token_images1/c5ca33bc.png)

1. 이더만 보냈는데 토큰을 지급받게 된 이유:
   * 토큰을 확인해 보면 증가되었을 것임

   * Filename: contracts/crowdsale/`Crowdsale.sol`:

      ```javascriopt
      function () external payable {
        buyTokens(msg.sender);
      }
      ```

   * 이름이 없는 함수를 fallback 함수라고 부름.
   * 컨트랙트는 한 개의 fallback을 가진 수 있음.
   * 트랜잭션이 컨트랙트에 이더를 송금했으나 메소드를 호출하지 않은 경우에 실행.
   * 이더를 전송만 했기 때문에 fallback 함수가 실행되고 buyTokens 함수를 호출하여 토큰을 구매하게 됨.
   * `payable` : 내장 modifier로 지정되지 않으면 모든 전송을 자동으로 Reject 시킴.
      * Default로 모든 function에서는 이더 전송에 관한 모든 것을 Reject.

### Truffle 을 사용하여 ICO 참여

[Debugging on IDE](/docs/h-chain/testing/debugging-on-ide.html) 를 참고하여 Truffle(Smart Contract Development Framework)을 사용하여 ICO 참여.

<!--
1. Blockchain Node Start:

1. Manage mongod Processes:
   * Start mongod as a Daemon(Background)

   ```shell
   sudo mongod --fork --logpath /var/log/mongodb.log
   mongo
   tail -f /var/log/mongodb.log
   sudo mongod --shutdown
   ```
   https://docs.mongodb.com/manual/tutorial/manage-mongodb-processes/

   `or`

   ```shell
   nohup mongod &
   jobs
   fg %1
   ```

https://bluebead38.blogspot.com/2017/07/python-mongod-error-insufficient-free.html


   * Stop mongod

   ```shell
   sudo mongod --fork --logpath /var/log/mongodb.log
   mongo
   tail -f /var/log/mongodb.log
   sudo mongod --shutdown
   ```




1. API Server Start:

   ```shell
   cd ~/hanwha-api
   npm run build
   forever start dist/index.js
   forever restart dist/index.js
   forever stop all
   ```
forever logs
info:    Logs for running Forever processes
data:        script        logfile
data:    [0] dist/index.js /home/bc/.forever/hUP4.log
tail -f /home/bc/.forever/hUP4.log

.env 의 OPERATOR_PRIVATE_KEY 에서 0x는 제거 해야 함

MongoDB GUI Client

https://docs.mongodb.com/compass/master/install/

> show dbs
admin       (empty)
hanwha-api  0.078GB
local       0.078GB
test        (empty)
> use hanwha-api
> show collections
system.indexes
tokens
> db.tokens.find().pretty();
https://docs.mongodb.com/manual/reference/method/cursor.pretty/

db.tokens.drop();


use hanwha-api
db.dropDatabase();


https://github.com/TylerBrock/mongo-hacker
cannot create mongo_hacker.js: Permission denied
sudo npm install --global --unsafe-perm mongo-hacker
    https://github.com/TylerBrock/mongo-hacker/issues/176




For those using ganache-cli
Ganache-cli has a default gasLimit of 0x6691b7,
so your gas value in truffle.js must not exceed it.
If you need a higher gasLimit, you need to set a higher gasLimit when starting ganache-cli
ganache-cli -l 8000000
https://github.com/trufflesuite/ganache-cli#command-line


https://ethereum.stackexchange.com/questions/29584/problem-connecting-testrpc-to-remix/29586#29586


netstat -nap
sudo iptables -I INPUT 1 -p tcp --dport 8545 -j ACCEPT

13.124.63.18

pragma solidity ^0.5.1;

import "https://github.com/OpenZeppelin/openzeppelin-solidity/contracts/examples/SimpleToken.sol";

contract TestToken is SimpleToken{

}


docker restart testrpc80



inflict input jaguar umbrella reward heart chat neck sort collect correct wrestle


-->

## References

1. [Session 1-2_201811_혁신부문 신기술DAY_세미나 최종취합본.pdf](https://sncsp.eagleoffice.co.kr/W/W00054/_layouts/15/WopiFrame.aspx?sourcedoc=/W/W00054/D00001/개발%20가이드%20배포판/Session%201-2_201811_혁신부문%20신기술DAY_세미나%20최종취합본.pdf&action=default&DefaultItemOpen=1)(홍민표 과장 작성, `비공개`) 일부 활용
