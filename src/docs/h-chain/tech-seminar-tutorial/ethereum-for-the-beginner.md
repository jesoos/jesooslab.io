---
title: H-Chain | 블록체인 입문자를 위한 이더리움 둘러보기
layout: docs.hbs
---

# Blockchain
![](.ethereum-for-the-beginner_images/page1.png)


# DLT

![](.ethereum-for-the-beginner_images/DLT.png)


# Blockchain Platform

![](.ethereum-for-the-beginner_images/blockplatform.png)


# Ehtereum

![](.ethereum-for-the-beginner_images/ethereum.png)


# Block & Chain

![](.ethereum-for-the-beginner_images/cdd202a9.png)


# Block Hash

![](.ethereum-for-the-beginner_images/blockhash.png)


# Mining

![](.ethereum-for-the-beginner_images/mining.png)


# Ethash

![](.ethereum-for-the-beginner_images/ethash.png)


# Fork

![](.ethereum-for-the-beginner_images/fork.png)


# Account State

![](.ethereum-for-the-beginner_images/accountState.png)


# Account State Detail

![](.ethereum-for-the-beginner_images/accountStateDetail.png)


# Transaction

![](.ethereum-for-the-beginner_images/transaction.png)


# Gas

![](.ethereum-for-the-beginner_images/gas.png)


# Block Detail

![](.ethereum-for-the-beginner_images/blockDetail.png)


