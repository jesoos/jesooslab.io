---
title: H-Chain | Solidity Exercise 토큰만들기
layout: docs.hbs
---
# 토큰 만들기
Solidity는 스마트 컨트랙트를 구현하기 위한 컨트랙트 기반의 고급 프로그래밍 언어이다. Solidity는 C++, Python, 그리고 JavaScript의 영향을 받아 만들어졌으며 Ethereum Virtual Machine(EVM)에서 구동되도록 설계되었다.

   ```javascript
    pragma solidity ^0.4.24;
    
    contract MyFirstCoin {
        string public name; // 토큰 이름
        string public symbol; // 토큰 단
        uint8 public decimals;  // 소수점 이하 자릿수위
        uint256 public totalSupply; // 총 발행량
    
        mapping (address=>uint256) public balanceOf; // 계정 당 잔고
    
        address public owner;
    
        modifier onlyOwner() {
            require(msg.sender == owner);
            _;
        }
        modifier checkValidate(address _to, uint256 _value) {
            require(balanceOf[msg.sender] >= _value);
            require(balanceOf[_to] + _value >= balanceOf[_to]);
            _;
        }
        event Transfer(address indexed from, address indexed to, uint256 value);
    
        function MyFirstCoin(uint256 _supply, string _name, string _symbol, uint8 _decimals) {
            balanceOf[msg.sender] = _supply;
            name = _name;
            symbol = _symbol;
            decimals = _decimals;
            totalSupply = _supply;
            owner = msg.sender;
        }
    
        function transfer(address _to, uint256 _value) public onlyOwner checkValidate(_to,_value) {
            balanceOf[msg.sender] -= _value;
            balanceOf[_to] += _value;
            Transfer(msg.sender, _to, _value);
        }
    }
   ```

   * 주요 코드 설명
   
   ```javascript
   mapping (address=>uint256) public balanceOf 
   ``` 
   : Key와 Value로 이루어진 자료 구조
    
   ```javascript
   event Transfer(address from, address to, uint256 value);
   ``` 
   : 트랜잭션의 로그를 출력하기 위해 작성하며 클라이언트에서 계약의 처리 추적할 수 있게 함
   
   ```javascript
   msg.sender
   ``` 
   : 계약을 생성하고 tx를 전송한 계정의 주소를 담고 있는 변수
      
   ```solidity
   modifier checkValidate(address _to, uint256 _value) {
    require(balanceOf[msg.sender] >= _value);
    require(balanceOf[_to] + _value >= balanceOf[_to]);
    _;
   }
   ``` 
   : 함수를 실행하기 전에 동작 조건을 확인하고 함수 실행을 제어하는 수식자
         
   ```javascript
    function transfer(address _to, uint256 _value) public onlyOwner checkValidate(_to,_value) {
     balanceOf[msg.sender] -= _value;
     balanceOf[_to] += _value;
     Transfer(msg.sender, _to, _value);
    }
   ``` 
   : 암호화페를 전송하기 위한 함수로, 송금처 주소(_to)와 금액(_value)를 인수로 사용해 송금 처리
   
   
   
# Remix에서 배포하기
   Solidity를 프로그래밍 할 수 있는 개발 툴으로 복잡한 설치가 필요없고 웹 브라우저에서 항상 최신 버전을 사용할 수 있다.
   ([Remix 바로가기](https://remix.ethereum.org))
   
   ### 1. 코드작성
   ![](.solidity-exercise-토큰만들기_images/myTokenCode.png)
   
   ### 2. 컴파일
   ![](.smart-contract-tutorial_images/컴파일.png)
         
    Compile version을 0.4.24으로 선택한 후 컴파일 버튼을 누른다.
      
   ### 3. VM 설정
   ![](.smart-contract-tutorial_images/remix_jvm.png)
         
    현재 따로 Geth를 구동하고 있지 않으므로 JavaScript VM 환경을 선택한다.
    (Local에서 Geth를 실행했을 Web3 Provider 으로 연결 가능하다.)
    
   ### 4. 배포
   ![](.solidity-exercise-토큰만들기_images/deployToken.png)
    
    토큰을 만들기 위해서 (총발행수량, 토큰이름, 토큰별칭, 소숫점 단위) 순으로 입력한 후 Deploy 버튼을 누른다.
   
   ![](.solidity-exercise-토큰만들기_images/checkDeploy.png)
   
    결과값으로 Transaction의 hash, 소모된 gas량 등의 정보를 확인 할 수 있다. 
    
   ### 5. 토큰 전송하기 
   ![](.solidity-exercise-토큰만들기_images/accounts.png)
   
    현재 Remix에서 제공해주는 JVM에서 5개의 계정이 만들어져있는 상태이다.
    토큰 전송을 위해 두 번째 계정의 주소 20자리를 복사한다.
   
   ![](.solidity-exercise-토큰만들기_images/transfer.png)
    
    transfer(수신자, 송금액)을 입력해준다.
    송금자는 JVM Account로 선택되어있는 계정이며 송금 받을 수신자의 계정 20자리 주소를 입력하고,
    전송할 금액 Wei(1 ETH = 1 x 10^18)을 입력해준다.

   ![](.solidity-exercise-토큰만들기_images/checkTranferTx.png)
   
    결과값으로 송금한 계정의 주소, 수신자 계정의 주소, 송금액 등의 정보를 알 수 있다.
    
   ![](.solidity-exercise-토큰만들기_images/receiverBalance.png) 
    
    수신한 계정의 잔고가 100으로 증가한 것을 알 수 있다.

