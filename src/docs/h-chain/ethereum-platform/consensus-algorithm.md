---
title: H-Chain | Consensus Algorithm
layout: docs.hbs
---
# Consensus Algorithm

```shell
puppeth
...
Which consensus engine to use? (default = clique)
 1. Ethash - proof-of-work
 2. Clique - proof-of-authority
> 2
...
```

## PoA

PoA (Proof-of-Authority)는 지분(stake)으로의 정체성(identity)에 기반한 합의 메커니즘을 통해 상대적으로 빠른 트랜잭션을 제공하는 블록 체인에 사용되는 알고리즘입니다..

### Proof-of-authority

In PoA-based networks, transactions and blocks are validated by approved accounts, known as validators. Validators run software allowing them to put transactions in blocks. The process is automated and does not require validators to be constantly monitoring their computers. It, however, does require maintaining the computer (the authority node) uncompromised. The term was coined by Gavin Wood, co-founder of Ethereum and Parity Technologies.

With PoA, individuals earn the right to become validators, so there is an incentive to retain the position that they have gained. By attaching a reputation to identity, validators are incentivized to uphold the transaction process, as they do not wish to have their identities attached to a negative reputation. This is considered more robust than PoS (proof-of-stake), as:

* In PoS, while a stake between two parties may be even, it does not take into account each party’s total holdings. This means that incentives can be unbalanced.
* Meanwhile, PoW uses an enormous amount of computing power, which, in itself lowers incentive. It is also vulnerable to attack, as a potential attacker would only need to have 51% of the mining resources (hashrate) to control a network, although this is not easy to do.
On the other hand, PoA only allows non-consecutive block approval from any one validator, meaning that the risk of serious damage is centralized to the authority node.

PoA is suited for both private networks and public networks, like POA Network, where trust is distributed.

### Comparisons

* The proof-of-work (PoW) consensus uses a mining mechanism, where PoA does not.
* The proof-of-stake (PoS) mechanism works using an algorithm that selects participants with the highest stakes as validators, assuming that the highest stakeholders are incentivized to ensure a transaction is processed. PoW works by verifying that work (mining) has been done before transactions are carried out.
* Meanwhile, PoA uses identity as the sole verification of the authority to validate, meaning that there is no need to use mining.
* Delegated proof-of-stake (DPoS) works using witnesses, who generate blocks. Witnesses are elected by stakeholders at a rate of one vote per share per witness. However, with PoA, the appointment of an authority is automatic, meaning that there can be no bias or uneven process caused by unequal stakes. In PoA, validators need to have their identity verified formally via DApps, and have this identity information available in the public domain for everyone to cross-reference.
* Since PoA security is centralized in the form of the authority node, a lot of PoA critics wonder the point of using a blockchain for this kind of usages - stating that PoA blockchains are not really decentralized and a simple DB would be more efficient, faster, and cost effective.

## References

1. https://en.wikipedia.org/wiki/Proof-of-authority
