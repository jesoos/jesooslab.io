---
title: H-Chain | Installation
layout: docs.hbs
---
# Installation

## Go-Ethereum (Geth) 구성도

![](.installation_images1/251e4d48.png)

서버에 직접 Geth를 설치하여 구성하는 방식을 설명한다.

* Puppeth 툴을 이용한 Genesis Block 생성
* Entry Node 구성
* Seal Node 구성

### Entry Node 구성(=Boot Node)

Entry Node는 API서버와 통신 및 타서버의 정보를 가지고 있는 서버이다.

1. Geth 및 Puppeth설치하기(root권한 필요, sudo 권한도 수행 가능)

   ```shell
   root@ip-10-20-1-59 :~# add-apt-repository -y ppa:ethereum/ethereum
   gpg: keyring `/tmp/tmp2zfhoeff/secring.gpg' created
   gpg: keyring `/tmp/tmp2zfhoeff/pubring.gpg' created
   gpg: requesting key 923F6CA9 from hkp server keyserver.ubuntu.com
   gpg: /tmp/tmp2zfhoeff/trustdb.gpg: trustdb created
   gpg: key 923F6CA9: public key "Launchpad PPA for Ethereum" imported
   gpg: Total number processed: 1
   gpg:               imported: 1  (RSA: 1)
   OK

   root@ip-10-20-1-59 :~# apt-get update; apt-get install -y ethereum
   Hit:1 http://ap-northeast-2.ec2.archive.ubuntu.com/ubuntu xenial InRelease
   Get:2 http://ap-northeast-2.ec2.archive.ubuntu.com/ubuntu xenial-updates InRelease [109 kB]
   Get:3 http://ap-northeast-2.ec2.archive.ubuntu.com/ubuntu xenial-backports InRelease [107 kB]
   Get:4 http://ap-northeast-2.ec2.archive.ubuntu.com/ubuntu xenial/main Sources [868 kB]
   Get:5 http://ap-northeast-2.ec2.archive.ubuntu.com/ubuntu xenial/restricted Sources [4,808 B]
   ……
   ……
   Setting up puppeth (1.8.12+build14270+xenial) ...
   Setting up rlpdump (1.8.12+build14270+xenial) ...
   Setting up wnode (1.8.12+build14270+xenial) ...
   Setting up ethereum (1.8.12+build14270+xenial) ...

   # Go-Ethereum (Geth 설치가 완료되었다.)
   ```

1. 이더리움에 사용할 계정 생성

   ```shell
   ubuntu@ip-10-20-1-59 :~# geth account new
   ```

   ![](.installation_images1/1c971a8d.png)

   ```shell
   ubuntu@ip-10-20-1-59 :~# geth account list
   ```

   ![](.installation_images1/81bb7608.png)

1. 제네시스 블록 생성 (puppeth 실행 or 제네시스 블록은 양식만 맞다면 텍스트 편집기로 만들어도 됨)

   ```shell
   ubuntu@ip-10-20-1-59 :~# puppeth
   ```

   ![](.installation_images1/525ed533.png)

1. 기본 정보를 입력하여 제네시스 블록 생성

   ```shell
   Please specify a network name to administer (no spaces or hyphens, please)
   > devnet

   Sweet, you can set this via --network=devnet next time!

   INFO [07-13|01:59:02.765] Administering Ethereum network           name=devnet
   WARN [07-13|01:59:02.765] No previous configurations found         path=/home/ubuntu/.puppeth/devnet

   What would you like to do? (default = stats)
    1. Show network stats
    2. Configure new genesis
    3. Track new remote server
    4. Deploy network components
   > 2

   Which consensus engine to use? (default = clique)
    1. Ethash - proof-of-work
    2. Clique - proof-of-authority
   > 2

   How many seconds should blocks take? (default = 15)
   > “Enter” or “시간입력”

   Which accounts are allowed to seal? (mandatory at least one)
   > 0xed5b60a5130fa5f3dc442b2e4f460158c8746c14                             # geth account new에서 생성된 계정 주소, Seal 계정
   > 0x

   Which accounts should be pre-funded? (advisable at least one)
   > 0xed5b60a5130fa5f3dc442b2e4f460158c8746c14
   > 0x

   Specify your chain/network ID if you want an explicit one (default = random)
   > 12345
   INFO [07-13|01:59:25.353] Configured new genesis block

   What would you like to do? (default = stats)
    1. Show network stats
    2. Manage existing genesis
    3. Track new remote server
    4. Deploy network components
   > 2

    1. Modify existing fork rules
    2. Export genesis configuration
    3. Remove genesis configuration
   > 2
   Which file to save the genesis into? (default = devnet.json)
   >
   INFO [07-13|01:59:30.004] Exported existing genesis block

   What would you like to do? (default = stats)
    1. Show network stats
    2. Manage existing genesis
    3. Track new remote server
    4. Deploy network components
   ^C                     #  Ctrl + C or exit 를 눌러 빠져나온다.

   ubuntu@ip-10-20-1-59:~$ ls
   devnet.json                                          # 생성된 제네시스 블록이 보인다.
   ```

### Entry Node 구성

1. Entry Node 실행

   ```shell
   ubuntu@ip-10-20-1-59:~$ geth init devnet.json
   ```

   ![](.installation_images1/555538ee.png)

   ```shell
   ubuntu@ip-10-20-1-59:~$ geth --networkid 12345 --rpc --rpcaddr 0.0.0.0 --rpcapi "db, eth, net, admin, personal, web3" --ws --wsport 8546 --wsaddr 0.0.0.0 --wsorigins="*"
   ```

   * Option 설명
      - `networkid` : 제네시스 블록에 입력된 networkid값을 입력한다.(Private BlockChain 망 구현을 위해 필요)
      - `rpc` : 원격 프로시져 호출을 위한 옵션이다.
      - `rpcaddr` : 서비스를 하는 IP 대역을 기재. (보통 자신의 서버를 의미)
      - `rpcapi` : rpc통신시 허용하는 명령어 그룹을 정의한다.
      - `ws` : 웹소켓 통신을 위한 옵션이다.
      - `wsport` : 웹소켓 포트를 정의한다.
      - `wsaddr` : 서비스를 하는 IP 대역을 기재. (보통 자신의 서버를 의미)
      - `wsorigins` : 웹소켓을 받아들일 Origin을 적는다.

### Seal Node 구성

1. 제네시스 블록 복사
   - Entry에서 생성한 제네시스 블록을 복제하여 Seal서버에 카피 한다.
   - ~/.ethereum/keystore에 모든 파일을 Seal의 같은 경로에 카피한다.

1. 제네시스 블록 초기화

   ```shell
   ubuntu@ip-10-20-1-227:~$ geth init devnet.json
   ```

   ![](.installation_images1/4a44137d.png)

1. Entry Node의 enode값 조회
   - enode란 서버간 연결을 위한 고유 주소로 이해하면 된다.

   ```shell
   ubuntu@ip-10-20-1-59 :~# geth --exec admin.nodeInfo.id attach
   ```

   ![](.installation_images1/09d51d52.png)

   ```shell
   # 6a~로 표시되어 있는 enode값은 Seal노드에서 Entry로 연결하기 위해 필요한 값이다. 서버마다 값은 다르게 표시된다.
   ```

1. signer.pass파일 생성
   - PoA는 블럭생성시 계정을 잠구어 버리므로, 명령어에 Account 패스워드를 미리 만들어 사용할 수 있게 한다.

   ```shell
   ubuntu@ip-10-20-1-227:~$ echo test > signer.pass         # test는 조금전 Entry에서 geth account new 생성시 입력한 패스워드 값이다.
   ```

1. Seal Node 실행

   ```shell
    ubuntu@ip-10-20-1-227:~$ geth --networkid 12345 --rpc --rpcaddr 0.0.0.0 --bootnodes \
    enode://6af5022697c7f5270c7ae40a60a3bdc567c020688698af46ca6733d5596f783e5ea72d1a9c87be0fba785e033dbe3b63ee5b8699634f6c619a4c91dde8bb4261@10.20.1.59:30303 \
    --etherbase 0xed5b60a5130fa5f3dc442b2e4f460158c8746c14 --unlock 0 --password ./signer.pass \
    --mine --minerthreads 1 --targetgaslimit 8000000 --gasprice 0 2
   ```

   * Option 설명
      - `networkid` : 제네시스 블록에 입력된 networkid값을 입력한다.(Private BlockChain 망 구현을 위해 필요)
      - `bootnodes` : Entry와 연결하기 위한 옵션이다.
      - `etherbase` : 마이닝할 이더 계정을 의미한다.
      - `unlock` : 이더계정을 unlock 하기 위한 옵션이다.
      - `mine` : Seal로 동작하기 위한 옵션이며, 이 후 실행 쓰레드 단위나 리밋을 정한다.

1. 종료시

   ```shell
   ubuntu@ip-10-20-1-227:~$ pkill -9 geth        # 단순 프로세스 형태로 수행되기에 kill 명령어로 종료한다.
   ```

### Ethereum Network ID

```shell
0: Olympic, Ethereum public pre-release testnet
1: Frontier, Homestead, Metropolis, the Ethereum public main network
1: Classic, the (un)forked public Ethereum Classic main network, chain ID 61
1: Expanse, an alternative Ethereum implementation, chain ID 2
2: Morden, the public Ethereum testnet, now Ethereum Classic testnet
3: Ropsten, the public cross-client Ethereum testnet
4: Rinkeby, the public Geth PoA testnet
8: Ubiq, the public Gubiq main network with flux difficulty chain ID 8
42: Kovan, the public Parity PoA testnet
77: Sokol, the public POA Network testnet
99: Core, the public POA Network main network
100: xDai, the public MakerDAO/POA Network main network
401697: Tobalaba, the public Energy Web Foundation testnet
7762959: Musicoin, the music blockchain
61717561: Aquachain, ASIC resistant chain
[Other]: Could indicate that your connected to a local development test network
```

## References

1. [Session 2_201811_혁신부문 신기술DAY_세미나 최종취합본.pdf](https://sncsp.eagleoffice.co.kr/W/W00054/_layouts/15/WopiFrame2.aspx?sourcedoc=/W/W00054/D00001/개발%20가이드%20배포판/Session%202_201811_혁신부문%20신기술DAY_세미나%20최종취합본.pdf&action=default&DefaultItemOpen=1)(장강홍 과장 작성, `비공개`) 기반으로 작성
