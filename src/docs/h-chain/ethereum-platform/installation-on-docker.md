---
title: H-Chain | Installation on Docker
layout: docs.hbs
---
# Installation on Docker

## 구성도

![](.installation_images1/22d74a8c.png)

## 사전 필요작업

Private Blockchain 망 구성에 아래의 작업은 사전에 이루어져야 한다.

1. 배포서버의 public key를 각 서버에 사전 등록해 놓아야 한다(ssh-keygen를 이용하여 생성).
1. Docker를 이용하므로, Docker 및 Docker-compose가 설치 되어야 한다.
1. geth 패키지를 컴파일 할 경우 Go-Lang을 설치해야 한다.
   - 그 외에는 Go-Ethereum 사이트의 Binary 파일 이용
1. AWS의 경우 userdata 기능을 이용하여, EC2 시작시 패키지가 설치된 인스턴스를 수행할 수 있다(패키지 설치 방법은 OS에 따라 차이가 있음).

   ```shell
   #!/bin/sh
   apt update;apt install -y docker.io docker-compose
   usermod -aG docker ubuntu
   add-apt-repository -y ppa:ethereum/ethereum
   apt-get update
   apt-get install -y ethereum
   ```

* GitHub주소 : https://github.com/ethereum/go-ethereum

## ssh-key 등록

배포서버(or PC)에서 각 서버로 배포하기 위해 key를 생성 후 배포한다.

1. Geth 및 Puppeth설치하기 (root권한 필요, sudo 권한도 수행 가능)

   ```shell
   ubuntu@ip-20-20-1-184:~$ ssh-keygen
   generating public/private rsa key pair.
   Enter file in which to save the key (/home/ubuntu/.ssh/id_rsa):
   Enter passphrase (empty for no passphrase):
   Enter same passphrase again:
   Your identification has been saved in /home/ubuntu/.ssh/id_rsa.
   Your public key has been saved in /home/ubuntu/.ssh/id_rsa.pub.
   The key fingerprint is:
   SHA256:9tHQiDHmJ13EnrcxM5cBTybWf/ztihQ5DSwII0JWxCg ubuntu@ip-20-20-1-184
   The key's randomart image is:
   +---[RSA 2048]----+
   | .+=+ o +  oo+oo |
   |E..... = * =o =o |
   | .      = *.+. o+|
   |         o +o+*.=|
   |        S . =..B+|
   |       . . . o. o|
   |          . .  . |
   |           . .  .|
   |            . .. |
   +----[SHA256]-----+
   ubuntu@ip-20-20-1-184:~$ cat .ssh/id_rsa.pub        # 해당 퍼블릭키를 각 서버에 모두 추가한다. ~/.ssh/authorized_keys 파일에 추가
   ssh-rsa AAAAB3NzaC1yc2EAABAAABAQDMxn8r3Z2rXgc6tcn8j3uSmzY5uYiuh3Ru7a6Y4AmSXePQgovexza13m2c7XrDETAtohgaC469blRL8XFIPT/q5qqPhcT/zM5S2I2CAAADAQRSYU/mOQT1gpR606R1oNiyzcoYb97JttbYJLUbmHo6qfnd+P91QYtd68+lrsODnNyR6ztcDje9u7bZNB0SsntZeV+P0gxeIcRf5lMKsf3U5EvMwm+bvqKpW1YynAvXm3hwmPmDugKVqMU8dHw3HdkCXq7vCTa7PeFlZGriAZKHj5ibBozW5/pzrIF896/KDqVPH9x3cFqQ/OWdkQsVdrsy8zeFvE2DIvzE+Vboc1LttQP2fB ubuntu@ip-20-20-1-184
   ```

## geth 계정생성

1. Geth에서 Seal(Miner역할) 계정을 생성한다.

   ```shell
   ubuntu@ip-20-20-1-184:~$ geth account new
   WARN [11-05|07:28:59.696] Sanitizing cache to Go's GC limits       provided=1024 updated=330
   INFO [11-05|07:28:59.697] Maximum peer count                       ETH=25 LES=0 total=25
   Your new account is locked with a password. Please give a password. Do not forget this password.
   Passphrase:
   Repeat passphrase:
   Address: {1abac6b75e9145db7d5744a05f4fd926f945cefe}
   ```

## 제네시스 파일 생성

1. 블록체인의 기본이 되는 genesis 블록을 생성한다.
   * 같은 Private Network을 구축하기 위해서는 genesis 블록이 같아야 한다.

   ```shell
   ubuntu@ip-20-20-1-184:~$ puppeth
   +-----------------------------------------------------------+
   | Welcome to puppeth, your Ethereum private network manager |
   |                                                           |
   | This tool lets you create a new Ethereum network down to  |
   | the genesis block, bootnodes, miners and ethstats servers |
   | without the hassle that it would normally entail.         |
   |                                                           |
   | Puppeth uses SSH to dial in to remote servers, and builds |
   | its network components out of Docker containers using the |
   | docker-compose toolset.                                   |
   +-----------------------------------------------------------+

   Please specify a network name to administer (no spaces or hyphens, please)
   >test
   Sweet, you can set this via --network=test next time!

   INFO [11-05|07:26:04.557] Administering Ethereum network           name=test
   WARN [11-05|07:26:04.557] No previous configurations found         path=/home/ubuntu/.puppeth/test

   What would you like to do? (default = stats)
    1. Show network stats
    2. Configure new genesis
    3. Track new remote server
    4. Deploy network components
   >2
   Which consensus engine to use? (default = clique)
    1. Ethash - proof-of-work
    2. Clique - proof-of-authority
   >2
   How many seconds should blocks take? (default = 15)
   > 15 # 블록 생성시간

   Which accounts are allowed to seal? (mandatory at least one)
   > 0x1abac6b75e9145db7d5744a05f4fd926f945cefe # 블록을 생성할 계정
   > 0x

   Which accounts should be pre-funded? (advisable at least one)
   > 0x1abac6b75e9145db7d5744a05f4fd926f945cefe # 이더를 제공할 수 있는 계정
   > 0x

   Specify your chain/network ID if you want an explicit one (default = random)
   > 12345 # Network ID
   INFO [11-05|07:29:42.952] Configured new genesis block

   What would you like to do? (default = stats)
    1. Show network stats
    2. Manage existing genesis
    3. Track new remote server
    4. Deploy network components
   >
   ```

## ethstats 배포

1. 블록체인 네트워크의 상태를 볼 수 있는 ethstats을 배포한다.

   ```shell
   What would you like to do? (default = stats)
    1. Show network stats
    2. Manage existing genesis
    3. Track new remote server
    4. Deploy network components
   > 4

   What would you like to deploy? (recommended order)
    1. Ethstats  - Network monitoring tool
    2. Bootnode  - Entry point of the network
    3. Sealer    - Full node minting new blocks
    4. Explorer  - Chain analysis webservice (ethash only)
    5. Wallet    - Browser wallet for quick sends
    6. Faucet    - Crypto faucet to give away funds
    7. Dashboard - Website listing above web-services
   > 1 # 나열된 순서대로 설치하는 것을 권장

   Which server do you want to interact with?
    1. Connect another server
   > 1

   What is the remote server's address ([username[:identity]@]hostname[:port])?
   > ubuntu@13.124.121.22

   The authenticity of host '13.124.121.22:22 (13.124.121.22:22)' can't be established.
   SSH key fingerprint is 9e:57:c4:6c:e9:87:c4:04:1f:19:10:bb:90:f4:61:3a [MD5]
   Are you sure you want to continue connecting (yes/no)? yes
   Which port should ethstats listen on? (default = 80)
   >

   Allow sharing the port with other services (y/n)? (default = yes)
   >
   INFO [11-05|07:40:12.851] Deploying nginx reverse-proxy            server=13.124.121.22 port=80
   Creating network "test_default" with the default driver
   Building nginx
   Step 1/1 : FROM jwilder/nginx-proxy
   latest: Pulling from jwilder/nginx-proxy
   Digest: sha256:e869d7aea7c5d4bae95c42267d22c913c46afd2dd8113ebe2a24423926ba1fff
   Status: Downloaded newer image for jwilder/nginx-proxy:latest
    ---> 0f74bf286b37
   Successfully built 0f74bf286b37
   Creating test_nginx_1

   Proxy deployed, which domain to assign? (default = 13.124.121.22)
   > # 여기에 입력한 주소를 웹페이지에 입력하면 화면을 볼 수 있다.

   What should be the secret password for the API? (must not be empty)
   > test
   Found orphan containers (test_nginx_1) for this project. If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
   Building ethstats
   Step 1/2 : FROM puppeth/ethstats:latest
   latest: Pulling from puppeth/ethstats
   Digest: sha256:1728c03555d3327f68be924116eed9f9de56671949c21505f4b78518f06e687e
   Status: Downloaded newer image for puppeth/ethstats:latest
   …..
   Removing intermediate container 80374b331ba5
   Successfully built 33d96e15dc41
   Creating test_ethstats_1

   ```

   ![](.installation_images1/16ad7de6.png)

## Entrynode(=bootnode) 배포

1. 노드간을 연결하며, API에서 Endpoint 역할을 하는 Entrynode를 설치한다.

   ```shell
   What would you like to do? (default = stats)
    1. Show network stats
    2. Manage existing genesis
    3. Manage tracked machines
    4. Manage network components
   > 4

    1. Tear down Nginx on ubuntu@13.124.121.22
    2. Tear down Ethstats on ubuntu@13.124.121.22
    3. Deploy new network component
   > 3

   What would you like to deploy? (recommended order)
    1. Ethstats  - Network monitoring tool
    2. Bootnode  - Entry point of the network
    3. Sealer    - Full node minting new blocks
    4. Explorer  - Chain analysis webservice (ethash only)
    5. Wallet    - Browser wallet for quick sends
    6. Faucet    - Crypto faucet to give away funds
    7. Dashboard - Website listing above web-services
   > 2

   Which server do you want to interact with?
    1. ubuntu@13.124.121.22
    2. Connect another server
   > 1

   Where should data be stored on the remote machine?
   > ~/bootnode # 정보 저장되는 디렉토리

   Which TCP/UDP port to listen on? (default = 30303)
   >

   How many peers to allow connecting? (default = 512)
   >

   How many light peers to allow connecting? (default = 256)
   >

   What should the node be called on the stats page?
   > Bootnode # ethstats에 표시되는 이름
   Found orphan containers (test_ethstats_1, test_nginx_1) for this project. If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
   Building bootnode
   Step 1/4 : FROM ethereum/client-go:latest
   latest: Pulling from ethereum/client-go
   ….
   Step 4/4 : ENTRYPOINT /bin/sh geth.sh
    ---> Running in 2cebb3de68c9
   Removing intermediate container 2cebb3de68c9
   Successfully built b4ff2a3ab577
   Creating test_bootnode_1
   ```

1. 이전에 생성한 ethstats 페이지에 가보면 Bootnode가 생성되어 있다.

   ![](.installation_images1/2028cbf0.png)

## Sealnode 배포

1. 노드간을 연결하며, API에서 Endpoint 역할을 하는 Entrynode를 설치한다.

   ```shell
   What would you like to do? (default = stats)
    1. Show network stats
    2. Manage existing genesis
    3. Manage tracked machines
    4. Manage network components
   > 4

    1. Tear down Nginx on ubuntu@13.124.121.22
    2. Tear down Ethstats on ubuntu@13.124.121.22
    3. Tear down Bootnode on ubuntu@13.124.121.22
    4. Deploy new network component
   > 4

   What would you like to deploy? (recommended order)
    1. Ethstats  - Network monitoring tool
    2. Bootnode  - Entry point of the network
    3. Sealer    - Full node minting new blocks
    4. Explorer  - Chain analysis webservice (ethash only)
    5. Wallet    - Browser wallet for quick sends
    6. Faucet    - Crypto faucet to give away funds
    7. Dashboard - Website listing above web-services
   > 3

   Which server do you want to interact with?
    1. ubuntu@13.124.121.22
    2. Connect another server
   > 1

   Where should data be stored on the remote machine?
   > ~/seal

   Which TCP/UDP port to listen on? (default = 30303)
   > 30304 # 해당 시연에서는 한 서버에 entry/sealnode를 구축하였으므로, 포트를 다르게 한다.

   How many peers to allow connecting? (default = 50)
   >

   How many light peers to allow connecting? (default = 0)
   >

   What should the node be called on the stats page?
   > Seal

   Please paste the signer's key JSON:
   > {"address":"1abac6b75e9145db7d5744a05f4fd926f945cefe","crypto":{"cipher":"aes-128-ctr","ciphertext":"1d162e2d97de7f2b1ed085ad260a07551539e36fe002687fe5954008f9a36e44","cipherparams":{"iv":"8364d3cb2eeb877c5ae5bba5cd1e53c4"},"kdf":"scrypt","kdfparams":{"dklen":32,"n":262144,"p":1,"r":8,"salt":"0fcb70d9ba5900a98e53cd09ae68413af2fcad7c3a2016d41162e3a82fe23464"},"mac":"e42c85197415537faa204f408afe4736e162d5f2dda7f44c3244c899e970df8b"},"id":"3b159c99-9973-468e-ba96-fbc95577ee85","version":3}

   What's the unlock password for the account? (won't be echoed)
   >

   What gas limit should empty blocks target (MGas)? (default = 7.500)
   >
   What gas limit should empty blocks target (MGas)? (default = 7.500)
   >

   What gas limit should full blocks target (MGas)? (default = 10.000)
   >

   What gas price should the signer require (GWei)? (default = 1.000)
   >
   Found orphan containers (test_bootnode_1, test_ethstats_1, test_nginx_1) for this project. If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
   Building sealnode
   Step 1/6 : FROM ethereum/client-go:latest
    ---> 214e8b03f1ec
   Step 2/6 : ADD genesis.json /genesis.json
    ---> Using cache
    ---> 8475c2668467
   Step 3/6 : ADD signer.json /signer.json
   …/signer.pass
    ---> 37ddc8ff8290
   Removing intermediate container 403679bc327d
   Step 5/6 : RUN echo 'geth --cache 512 init /genesis.json' > geth.sh &&  echo 'mkdir -p /root/.ethereum/keystore/ && cp /signer.json /root/.ethereum/keystore/' >> geth.sh &&    echo $'exec geth --networkid 12345 --cache 512 --port 30304 --nat extip:13.124.121.22 --maxpeers 50  --ethstats \'Seal:test@13.124.121.22\' --bootnodes enode://e7f1c45420ef4dfa1be636adcc6abb8fb582918b1256a7f5745be7efd3f170d8b1f4d29be6987ac49d3ddc47086b0dc207000273c31fea3addd327553acec9e2@13.124.121.22:30303  --unlock 0 --password /signer.pass --mine --miner.gastarget 7500000 --miner.gaslimit 10000000 --miner.gasprice 1000000000' >> geth.sh
    …
   Removing intermediate container 1c85d499c8e9
   Successfully built afc3e0343636
   Creating test_sealnode_1
   ```

1. 이전에 생성한 ethstats 페이지에 가보면 Sealnode가 추가 생성되어 있고, 추가로 node생성과 함께 아까 없던 정보가 보이며, 블록이 생성되고 있다.

   ![](.installation_images1/bb9d8709.png)

## Wallet 배포

1. 사용자에게 개인지갑 주소를 제공한다.

   ```shell
   What would you like to do? (default = stats)
    1. Show network stats
    2. Manage existing genesis
    3. Manage tracked machines
    4. Manage network components
   > 4

    1. Tear down Nginx on ubuntu@13.124.121.22
    2. Tear down Ethstats on ubuntu@13.124.121.22
    3. Tear down Bootnode on ubuntu@13.124.121.22
    4. Tear down Sealnode on ubuntu@13.124.121.22
    5. Deploy new network component
   > 5

   What would you like to deploy? (recommended order)
    1. Ethstats  - Network monitoring tool
    2. Bootnode  - Entry point of the network
    3. Sealer    - Full node minting new blocks
    4. Explorer  - Chain analysis webservice (ethash only)
    5. Wallet    - Browser wallet for quick sends
    6. Faucet    - Crypto faucet to give away funds
    7. Dashboard - Website listing above web-services
   > 5

   Which server do you want to interact with?
    1. ubuntu@13.124.121.22
    2. Connect another server
   > 2

   What is the remote server's address ([username[:identity]@]hostname[:port])?
   > ubuntu@52.78.143.118
   Which port should the wallet listen on? (default = 80)
   >

   Allow sharing the port with other services (y/n)? (default = yes)
   >
   INFO [11-05|07:58:30.440] Deploying nginx reverse-proxy            server=52.78.143.118        port=80
   Creating network "test_default" with the default driver
   Building nginx
   Step 1/1 : FROM jwilder/nginx-proxy
   latest: Pulling from jwilder/nginx-proxy
   Digest: sha256:e869d7aea7c5d4bae95c42267d22c913c46afd2dd8113ebe2a24423926ba1fff
   Status: Downloaded newer image for jwilder/nginx-proxy:latest
    ---> 0f74bf286b37
   Successfully built 0f74bf286b37
   Creating test_nginx_1

   Proxy deployed, which domain to assign? (default = 52.78.143.118)
   >

   Where should data be stored on the remote machine?
   > ~/wallet

   Which TCP/UDP port should the backing node listen on? (default = 30303)
   >

   Which port should the backing RPC API listen on? (default = 8545)
   >

   What should the wallet be called on the stats page?
   > Wallet
   Found orphan containers (test_nginx_1) for this project. If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
   Building wallet
   Step 1/5 : FROM puppeth/wallet:latest
   latest: Pulling from puppeth/wallet
   Digest: sha256:7027adeda7e93ffe456d65cf44d99427452050dec79579ea50701ea5edfaa835
   Status: Downloaded newer image for puppeth/wallet:latest
    ---> 1f6b340870f6
   Step 2/5 : ADD genesis.json /genesis.json
    ---> f5de4af19d05
   Removing intermediate container 7a1753b0ecf3
   Step 3/5 : RUN echo 'node server.js &'                     > wallet.sh &&       echo 'geth --cache 512 init /genesis.json' >> wallet.sh &&      echo $'exec geth --networkid 12345 --port 30303 --bootnodes enode://e7f1c45420ef4dfa1be636adcc6abb8fb582918b1256a7f5745be7efd3f170d8b1f4d29be6987ac49d3ddc47086b0dc207000273c31fea3addd327553acec9e2@13.124.121.22:30303 --ethstats \'Wallet:test@13.124.121.22\' --cache=512 --rpc --rpcaddr=0.0.0.0 --rpccorsdomain "*" --rpcvhosts "*"' >> wallet.sh
    ---> Running in 49ea80cb1566
    ---> a50638d0d219
   Removing intermediate container 49ea80cb1566
   Step 4/5 : RUN sed -i 's/PuppethNetworkID/12345/g' dist/js/etherwallet-master.js &&     sed -i 's/PuppethNetwork/TEST/g'     dist/js/etherwallet-master.js &&   sed -i 's/PuppethDenom/TEST/g'         dist/js/etherwallet-master.js &&  sed -i 's/PuppethHost/52.78.143.118/g'           dist/js/etherwallet-master.js &&       sed -i 's/PuppethRPCPort/8545/g'     dist/js/etherwallet-master.js
    ---> Running in b4050985d7fb
    ---> 478f1fc8a56a
   Removing intermediate container b4050985d7fb
   Step 5/5 : ENTRYPOINT /bin/sh wallet.sh
    ---> Running in f9acae02025c
    ---> cbed83cbb3ca
   Removing intermediate container f9acae02025c
   Successfully built cbed83cbb3ca
   Creating test_wallet_1
   ```

1. Wallet의 ip주소로 접속해보면 배포된 페이지를 볼 수 있다. 또한 ethstat에서 Wallet도 같이 표시된다.

   ![](.installation_images1/83cf28b9.png)
   ![](.installation_images1/0e4ca573.png)

## Faucet 배포

1. 사용자에게 테스트 할 수 있도록 이더를 제공하는 역할을 한다.

   ```shell
   What would you like to do? (default = stats)
    1. Show network stats
    2. Manage existing genesis
    3. Manage tracked machines
    4. Manage network components
   > 4

    1. Tear down Nginx on ubuntu@52.78.143.118
    2. Tear down Wallet on ubuntu@52.78.143.118
   …..
    5. Tear down Sealnode on ubuntu@13.124.121.22
    6. Tear down Nginx on ubuntu@13.124.121.22
    7. Deploy new network component
   > 7

   What would you like to deploy? (recommended order)
    1. Ethstats  - Network monitoring tool
    2. Bootnode  - Entry point of the network
    3. Sealer    - Full node minting new blocks
   ……
    7. Dashboard - Website listing above web-services
   > 6

   Which server do you want to interact with?
    1. ubuntu@13.124.121.22
    2. ubuntu@52.78.143.118
    3. Connect another server
   > 3

   What is the remote server's address ([username[:identity]@]hostname[:port])?
   > ubuntu@13.124.197.217
   Which port should the faucet listen on? (default = 80)
   >

   Allow sharing the port with other services (y/n)? (default = yes)
   >
   INFO [11-05|08:04:26.864] Deploying nginx reverse-proxy            server=13.124.197.217       port=80
   Creating network "test_default" with the default driver
   Building nginx
   Step 1/1 : FROM jwilder/nginx-proxy
   latest: Pulling from jwilder/nginx-proxy
   Digest: sha256:e869d7aea7c5d4bae95c42267d22c913c46afd2dd8113ebe2a24423926ba1fff
   Status: Downloaded newer image for jwilder/nginx-proxy:latest
    ---> 0f74bf286b37
   Successfully built 0f74bf286b37
   Creating test_nginx_1

   Proxy deployed, which domain to assign? (default = 13.124.197.217)
   >

   How many Ethers to release per request? (default = 1)
   >

   How many minutes to enforce between requests? (default = 1440)
   >

   How many funding tiers to feature (x2.5 amounts, x3 timeout)? (default = 3)
   >

   Enable reCaptcha protection against robots (y/n)? (default = no)
   >
   WARN [11-05|08:04:53.665] Users will be able to requests funds via automated scripts
   Where should data be stored on the remote machine?
   > ~/faucet

   Which TCP/UDP port should the light client listen on? (default = 30303)
   >

   What should the node be called on the stats page?
   > Faucet

   Please paste the faucet's funding account key JSON:
   > {"address":"1abac6b75e9145db7d5744a05f4fd926f945cefe","crypto":{"cipher":"aes-128-ctr","ciphertext":"1d162e2d97de7f2b1ed085ad260a07551539e36fe002687fe5954008f9a36e44","cipherparams":{"iv":"8364d3cb2eeb877c5ae5bba5cd1e53c4"},"kdf":"scrypt","kdfparams":{"dklen":32,"n":262144,"p":1,"r":8,"salt":"0fcb70d9ba5900a98e53cd09ae68413af2fcad7c3a2016d41162e3a82fe23464"},"mac":"e42c85197415537faa204f408afe4736e162d5f2dda7f44c3244c899e970df8b"},"id":"3b159c99-9973-468e-ba96-fbc95577ee85","version":3}

   What's the unlock password for the account? (won't be echoed)
   >

   Permit non-authenticated funding requests (y/n)? (default = false)
   > y
   Found orphan containers (test_nginx_1) for this project. If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
   Building faucet
   Step 1/6 : FROM ethereum/client-go:alltools-latest
   …..
   Removing intermediate container 29c06e5e6414
   Step 6/6 : ENTRYPOINT faucet --genesis /genesis.json --network 12345 --bootnodes ..
   Removing intermediate container f6d40f4c3c5a
   Successfully built cfc398966410
   Creating test_faucet_1
   ```

1. Faucet의 ip주소로 접속해보면 배포된 페이지를 볼 수 있다. 또한 ethstat에서 Faucet도 같이 표시된다.

   ![](.installation_images1/9868289c.png)
   ![](.installation_images1/347e034c.png)

## Dashboard 배포

1. Ethstats, Wallet, Faucet 등이 별도의 웹페이지로 보여지게 되는데 이것을 한화면에 표시해준다. But, 사이드 메뉴외에 별다른 기능은 없다.

   ```shell
   What would you like to deploy? (recommended order)
    1. Ethstats  - Network monitoring tool
    2. Bootnode  - Entry point of the network
    3. Sealer    - Full node minting new blocks
    4. Explorer  - Chain analysis webservice (ethash only)
    5. Wallet    - Browser wallet for quick sends
    6. Faucet    - Crypto faucet to give away funds
    7. Dashboard - Website listing above web-services
   > 7

   Which server do you want to interact with?
    1. ubuntu@13.124.121.22
    2. ubuntu@13.124.197.217
    3. ubuntu@52.78.143.118
    4. Connect another server
   > 4

   What is the remote server's address ([username[:identity]@]hostname[:port])?
   > ubuntu@13.125.200.241

   The authenticity of host '13.125.200.241:22 (13.125.200.241:22)' can't be established.
   SSH key fingerprint is 99:d7:6a:47:4f:68:10:83:6a:80:ef:96:38:5d:2b:19 [MD5]
   Are you sure you want to continue connecting (yes/no)? yes

   Which port should the dashboard listen on? (default = 80)
   >
   Allow sharing the port with other services (y/n)? (default = yes)
   >
   INFO [11-05|08:36:57.376] Deploying nginx reverse-proxy            server=13.125.200.241        port=80
   Creating network "test_default" with the default driver
   Building nginx
   Step 1/1 : FROM jwilder/nginx-proxy
   latest: Pulling from jwilder/nginx-proxy
   Digest: sha256:e869d7aea7c5d4bae95c42267d22c913c46afd2dd8113ebe2a24423926ba1fff
   Status: Downloaded newer image for jwilder/nginx-proxy:latest
    ---> 0f74bf286b37
   Successfully built 0f74bf286b37
   Creating test_nginx_1

   Proxy deployed, which domain to assign? (default = 13.125.200.241)
   >

   Which ethstats service to list? (default = 13.124.121.22)
    1. 13.124.121.22
    2. List external ethstats service
    3. Don't list any ethstats service
   > 1

   Which explorer service to list? (default = don't list)
    1. List external explorer service
    2. Don't list any explorer service
   > 2

   Which wallet service to list? (default = 52.78.143.118)
    1. 52.78.143.118
    2. List external wallet service
    3. Don't list any wallet service
   > 1
   Which faucet service to list? (default = 13.124.197.217)
    1. 13.124.197.217
    2. List external faucet service
    3. Don't list any faucet service
   > 1

   Include ethstats secret on dashboard (y/n)? (default = yes)
   > y
   Found orphan containers (test_nginx_1) for this project. If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
   Building dashboard
   Step 1/11 : FROM mhart/alpine-node:latest
   latest: Pulling from mhart/alpine-node
   Digest: sha256:544e9b97f5e1f4b93618d949cbdc9097cb79caf053c77aefccbda1463d71409f
   Status: Downloaded newer image for mhart/alpine-node:latest
   …….
   ….…
   baccb5d79543
   Step 9/11 : ADD puppeth.png /dashboard/puppeth.png
    ---> 6a63ca225fa8
   Removing intermediate container 605d4f66b3e2
   Step 10/11 : EXPOSE 80
    ---> Running in 9faa834b97f0
    ---> 824213d48918
   Removing intermediate container 9faa834b97f0
   Step 11/11 : CMD node /server.js
    ---> Running in 14b424df7a96
    ---> f6940d1c3332
   Removing intermediate container 14b424df7a96
   Successfully built f6940d1c3332
   Creating test_dashboard_1
   ```

1. Dashboard 페이지를 접속하면 지금까지 만든 화면을 모두 볼 수 있다. 앞에서도 말했지만 사이드 메뉴외 별다른 기능은 없다.

   ![](.installation_images1/96de20a9.png)

## References

1. [Session 2_201811_혁신부문 신기술DAY_세미나 최종취합본.pdf](https://sncsp.eagleoffice.co.kr/W/W00054/_layouts/15/WopiFrame2.aspx?sourcedoc=/W/W00054/D00001/개발%20가이드%20배포판/Session%202_201811_혁신부문%20신기술DAY_세미나%20최종취합본.pdf&action=default&DefaultItemOpen=1)(장강홍 과장 작성, `비공개`) 기반으로 작성
