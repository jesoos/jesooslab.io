---
title: H-Chain | Geth Options
layout: docs.hbs
---
# Geth Options

   ```shell
   ubuntu@ip-10-20-1-59:~$ geth --networkid 12345 --rpc --rpcaddr 0.0.0.0 --rpcapi "db, eth, net, admin, personal, web3" --ws --wsport 8546 --wsaddr 0.0.0.0 --wsorigins="*"
   ```

   * Option 설명
      - `networkid` : 제네시스 블록에 입력된 networkid값을 입력한다.(Private BlockChain 망 구현을 위해 필요)
      - `rpc` : 원격 프로시져 호출을 위한 옵션이다.
      - `rpcaddr` : 서비스를 하는 IP 대역을 기재. (보통 자신의 서버를 의미)
      - `rpcapi` : rpc통신시 허용하는 명령어 그룹을 정의한다.
      - `ws` : 웹소켓 통신을 위한 옵션이다.
      - `wsport` : 웹소켓 포트를 정의한다.
      - `wsaddr` : 서비스를 하는 IP 대역을 기재. (보통 자신의 서버를 의미)
      - `wsorigins` : 웹소켓을 받아들일 Origin을 적는다.

   ```shell
    ubuntu@ip-10-20-1-227:~$ geth --networkid 12345 --rpc --rpcaddr 0.0.0.0 --bootnodes \
    enode://6af5022697c7f5270c7ae40a60a3bdc567c020688698af46ca6733d5596f783e5ea72d1a9c87be0fba785e033dbe3b63ee5b8699634f6c619a4c91dde8bb4261@10.20.1.59:30303 \
    --etherbase 0xed5b60a5130fa5f3dc442b2e4f460158c8746c14 --unlock 0 --password ./signer.pass \
    --mine --minerthreads 1 --targetgaslimit 8000000 --gasprice 0 2
   ```

   * Option 설명
      - `networkid` : 제네시스 블록에 입력된 networkid값을 입력한다.(Private BlockChain 망 구현을 위해 필요)
      - `bootnodes` : Entry와 연결하기 위한 옵션이다.
      - `etherbase` : 마이닝할 이더 계정을 의미한다.
      - `unlock` : 이더계정을 unlock 하기 위한 옵션이다.
      - `mine` : Seal로 동작하기 위한 옵션이며, 이 후 실행 쓰레드 단위나 리밋을 정한다.

## References

1. [Session 2_201811_혁신부문 신기술DAY_세미나 최종취합본.pdf](https://sncsp.eagleoffice.co.kr/W/W00054/_layouts/15/WopiFrame2.aspx?sourcedoc=/W/W00054/D00001/개발%20가이드%20배포판/Session%202_201811_혁신부문%20신기술DAY_세미나%20최종취합본.pdf&action=default&DefaultItemOpen=1)(장강홍 과장 작성, `비공개`) 기반으로 작성
