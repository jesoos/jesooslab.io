---
title: H-Chain | Overview
layout: docs.hbs
---
# Ethereum Platform Overview

## Geth(Go-Ethereum)을 이용한 Private Network 구축 하기

### Goal(목적)

Geth(Go-Ethereum)를 통한 Private Network를 구축한다.

(서버 설치는 ubunut linux 16.04 기준으로 리눅스 계열 및 버전에 따라 방법의 차이는 존재한다.)

### Scope(적용 범위)

블록체인 플랫폼을 구성 후 이용을 원할 경우 웹이나 모바일을 구현 후 API통해 통신한다.

### Firewall(방화벽)

아래의 리스트의 방화벽은 서버간 오픈되어 있어야 한다.

22`SSH` 3000`geth` 8545`rpc` 30303-30305`geth` 80`web`

### Servers(필요 서버)

![](.installation_images1/3e42bd28.png)

## 구성요소

Private Blockchain 망 구성에 필요한 요소와 구성순서는 다음과 같다.

1. `genesis.json` - 프라이빗 체인의 최초블록(Genesis block)을 정의하는 파일, 같은 프라이빗 체인에 연결 하려는 노드들은 같은 제네시스 파일로 초기화 되어야 함
1. `ethstat` - 프라이빗 체인의 블록 넘버, 네트워크 상태 등을 모니터링하는 화면
1. `entrynode` - 노드를 연결하는 구심점 같은 역할 (=bootnode)
1. `sealnode` - 블록을 생성하는 노드
1. `walletnode` - 이더를 쉽게 전송할 수 있게 개인 지갑을 생성 해주는 기능을 수행하는 노드
1. `faucetnode` - 수수료가 모자라거나 할때 약간의 이더를 공급해주는 노드
1. `dashboard` - 웹페이지를 사용하는 기능(ethstat, wallet, faucet)을 하나의 화면에서 관리 할 수 있게 해주는 기능

## References

1. [Session 2_201811_혁신부문 신기술DAY_세미나 최종취합본.pdf](https://sncsp.eagleoffice.co.kr/W/W00054/_layouts/15/WopiFrame2.aspx?sourcedoc=/W/W00054/D00001/개발%20가이드%20배포판/Session%202_201811_혁신부문%20신기술DAY_세미나%20최종취합본.pdf&action=default&DefaultItemOpen=1)(장강홍 과장 작성, `비공개`) 기반으로 작성
