---
title: H-Chain | Application 개발 및 테스트
layout: docs.hbs
---
# Application 개발 및 테스트

Application 에서 API 서버의 Restful API를 호출하여 Blockchain 과 통신

1. API 서버의 Restful API를 호출하여 Blockchain 과 통신한다
   * Application 샘플 honechain_sample.html 참고

   ![](.application-개발-및-테스트_images1/e86a22ad.png)

   File: `./honechain_sample.html`

   ```javascript
   $.ajax({
       url: BASE_URL + 'honechain-sample/' + $('#iptDeployAddress').val().trim(),
       type: 'POST',
       dataType: 'json',
       contentType: 'application/json',
       cache: false,
       data:  JSON.stringify({
           "method": "doSum",
           "from": $('#iptAddress').val(),
           "address": $('#iptDeployAddress').val().trim(),
           "params": {
               "x": parseInt($('#iptOperand1').val()),
               "y": parseInt($('#iptOperand2').val())
           }
       }),
       success: function(json) {
           var responseMap = json.response;
           var responseJson = JSON.stringify(responseMap);

           $('#taRawTx').text(responseJson);
           transSignedTransaction(responseMap);
       }, error: function(xhr, status, error){
           alert("error doSum");
       }
   });
   ```

### 참고 파일 및 샘플

* Application : honechain_smaple.html
* API 서버 : hanwha-api-sample.zip
* Postman Script : HonechainSmaple Test.postman_collection.json
* Smart Contract Truffle Project : honechain-contract-sample.zip

## References

1. [Honechain 따라하기_v0.9.pdf](https://sncsp.eagleoffice.co.kr/W/W00054/_layouts/15/WopiFrame2.aspx?sourcedoc=/W/W00054/D00001/개발%20가이드%20배포판/Honechain%20따라하기_v0.9.pdf&action=default&DefaultItemOpen=1)(박강훈 과장 작성,`비공개`) 기반으로 작성
