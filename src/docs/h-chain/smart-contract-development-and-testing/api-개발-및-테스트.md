---
title: H-Chain | API 개발 및 테스트
layout: docs.hbs
---
# API 개발 및 테스트

API서버에 대한 상세 내용은 별도의 문서를 참고

1. Local에서 API 기동을 위한 환경설정
   * Blockchain Node 지정 및 관리자 API에서 사용할 계정과 Private Key를 .env 파일에 설정

   ![](.api-개발-및-테스트_images1/92894aac.png)

   ```shell
   tree -N -a -L 1 --dirsfirst
   .
   ├── contracts
   ├── dist
   ├── node_modules
   ├── src
   ├── .babelrc
   ├── `.env`
   ├── README.md
   ├── package-lock.json
   └── package.json
   ```

   ![](.api-개발-및-테스트_images1/f137d228.png)

   File: `.env`

   ```javascript
   CLIENT_NODE="http://127.0.0.1:7545"
   OPERATOR_ADDRESS="0x86Af84524A6ec6aDa653dfd1823201E1659F8286"
   OPERATOR_PRIVATE_KEY="6dfacf065bd766cae34c5d024436b44f55fc7dbad5224c125c6d216471a11285"
   ```
1. truffle compile 명령어의 결과물인 .json 파일을 API 서버의 contracts 폴더에 복사한다

   ![](.api-개발-및-테스트_images1/c8069188.png)

   Truffle Project :

   ```shell
   tree -N -a -L 2 --dirsfirst
   .
   ├── build
   │   └── contracts
   ├── contracts
   │   ├── `HonechainSample.sol`
   │   └── Migrations.sol
   ├── migrations
   │   ├── 1_initial_migration.js
   │   └── 2_sample_migration.js
   ├── test
   │   └── test.js
   ├── truffle-config.js
   └── truffle.js
   ```

   ![](.api-개발-및-테스트_images1/a17d65f7.png)

   API Server:

   ```shell
   tree -N -a -L 2 --dirsfirst
   .
   ├── contracts
   │   └── `HonechainSample.json`
   ```

1. `.json` 파일의 ABI, bytes code를 이용해 Contract를 배포하고 함수를 호출하여 사용할 수 있는 API를 만든다
   * 사용자 API - Private Key를 사용자가 가지고 있을 경우를 위한 API : Tx 서명은 사용자가 직접 수행해야 함
      - 서명을 할 수 있는 RawTx 정보를 리턴해주는 API
      - 서명된 Tx를 Blockchain에 전송해주는 API
   * 관리자 API - Private Key를 서버가 가지고 있을 경우를 위한 API : Tx 서명을 서버에서 할 수 있음
      - Contract 배포를 위한 API
      - 전달 된 요청을 서명하고 Blockchain에 전송해주는 API

   ![](.api-개발-및-테스트_images1/32f87642.png)

   ```shell
   tree -N -a -L 3 --dirsfirst
   ├── src
   │   ├── api
   │   │   ├── `honechain-sample.js` ----> "사용자 API"
   │   │   └── `operators.js` -----------> "관리자 API"
   │   ├── config.json
   │   └── index.js
   ├── .babelrc
   ```

1. web3.js 와 ethereumjs-tx 를 이용해 Blockchain Node 와 통신하기 위한 코드를 작성한다
   * [web3.js 관련내용](https://web3js.readthedocs.io/en/1.0/#) 참고
   * [ethereumjs-tx 관련내용](https://github.com/ethereumjs/ethereumjs-tx) 참고
   * API 구현 샘플 hanwha-api-sample.zip 참고

   ![](.api-개발-및-테스트_images1/78258cb8.png)

   Filename: `honechain-sample.js` <서명할 RawTx 리턴>

   ```javascript
   return res.status(200).json({
     code: 0,
     message: 'success',
     response: {
       nonce: nonce,
       chainId: await web3.eth.net.getId(),
       to: req.params.address,
       data: bytecode,
       value: req.body.value,
       gasPrice: '22e9',
       gas: parseInt(gas * 1.2),
     },
   });
   ```

   ![](.api-개발-및-테스트_images1/65a97ed3.png)

   Filename: `operators.js` <Tx 서명 후 전송>

   ```javascript
   const tx = new Tx(rawTx);
   tx.sign(privateKey);
   const serializedTx = tx.serialize().toString('hex');

   try {
     web3.eth.sendSignedTransaction('0x' + serializedTx)
     .on('receipt', receipt => {
       return res.status(200).json({
         code: 0,
         message: 'success',
         response: {
           txhash: receipt.transactionHash,
           contract_address: receipt.contractAddress,
         }
       });
     })
   ```

1. npm run start 로 API 서버를 기동한다
   * 기본적으로 Localhost 8080 포트로 API 서버가 기동된다
   * API 서버 환경 셋팅 관련 내용은 별도의 문서를 참고(.env 파일 수정 필요)

   ![](.api-개발-및-테스트_images1/334f0b2d.png)

   ```shell
   npm run start

   > hanwha-api@1.0.0 start /hanwha-api
   > npm run build && node dist/index.js

   > hanwha-api\@1.0.0 build /hanwha-api
   > babel src -s -D -d dist

   src/api/honechain-sample.js -> dist/api/honechain-sample.js
   src/api/operators.js -> dist/api/operators.js
   src/index.js -> dist/index.js
   Started on port 8080
   ```

1. Postman 으로 만들어진 API 를 호출해본다
   * Postman API 스크립트 샘플 HonechainSmaple `Test.postman_collection.json` 참고

   ![](.api-개발-및-테스트_images1/f408f1c3.png)

   ![](.api-개발-및-테스트_images1/1aef46c9.png)

## References

1. [Honechain 따라하기_v0.9.pdf](https://sncsp.eagleoffice.co.kr/W/W00054/_layouts/15/WopiFrame2.aspx?sourcedoc=/W/W00054/D00001/개발%20가이드%20배포판/Honechain%20따라하기_v0.9.pdf&action=default&DefaultItemOpen=1)(박강훈 과장 작성,`비공개`) 기반으로 작성
