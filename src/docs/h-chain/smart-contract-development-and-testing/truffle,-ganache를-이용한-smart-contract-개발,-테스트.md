---
title: H-Chain | Truffle, Ganache를 이용한 Smart Contract 개발, 테스트
layout: docs.hbs
---
# Truffle, Ganache를 이용한 Smart Contract 개발, 테스트

Truffle Framework, Ganache 에 대한 설치 방법 및 상세 내용은 Truffle 홈페이지 참고 (https://truffleframework.com)

1. Smart Contract 개발을 진행할 폴더 생성 후 truffle init 명령어 수행:
   * truffle project 기본 폴더들이 생성된다

   ![](.truffle,-ganache를-이용한-smart-contract-개발,-테스트_images1/2421aa0e.png)
   ![](.truffle,-ganache를-이용한-smart-contract-개발,-테스트_images1/f8f5a617.png)

1. project config 수정 및 Solidity 파일(.sol) 생성/개발:
   * Local의 Ganache와 연결될 수 있도록 환경설정 파일 수정
   * contracts 폴더에 Solidity 파일 생성
   * 구현 할 Smart Contract 내용을 Solidity 문법에 맞게 작성한다

   ![](.truffle,-ganache를-이용한-smart-contract-개발,-테스트_images1/e7b96ed1.png)
   ![](.truffle,-ganache를-이용한-smart-contract-개발,-테스트_images1/6d55b49f.png)
   ![](.truffle,-ganache를-이용한-smart-contract-개발,-테스트_images1/7e8620ab.png)

1. truffle compile 명렁어로 solidity 코드 컴파일:
   * build/contracts 폴더에 contract 이름으로 .json 파일이 만들어 진다(향후 API 서버에서 사용)
   * .json 파일에는 contract를 배포, 함수 호출 등을 위한 ABI 와 bytes 코드가 포함되어 있다

   ![](.truffle,-ganache를-이용한-smart-contract-개발,-테스트_images1/1f8200c6.png)
   ![](.truffle,-ganache를-이용한-smart-contract-개발,-테스트_images1/5fabeaa9.png)

1. Contract 배포를 위해 migrations 폴더 하위에 배포를 위한 파일을 생성하고 truffle migrate 명령어로 Contract를 배포한다:
   * 배포를 위한 파일 작성에 대한 내용은 truffle 홈페이지를 참고
   * 사용할 Blockchain Node로 지정한 Ganache에 Contract가 배포된다

   ![](.truffle,-ganache를-이용한-smart-contract-개발,-테스트_images1/6dbf1602.png)

   ![](.truffle,-ganache를-이용한-smart-contract-개발,-테스트_images1/abdbe2ca.png)
   ![](.truffle,-ganache를-이용한-smart-contract-개발,-테스트_images1/146a1cee.png)

   * Ganache GUI – Contract 배포 완료:

   ![](.truffle,-ganache를-이용한-smart-contract-개발,-테스트_images1/a7a285f1.png)

1. test를 위한 파일 생성/작성 후 truffle test 명령어로 작성한 테스트 코드를 수행하여 작성한 contract 를 검증한다:
   * 테스트 코드 작성에 대한 내용은 truffle 홈페이지를 참고
   * 테스트 코드를 통해 contract 가 의도대로 동작하는지 확인한다

   ![](.truffle,-ganache를-이용한-smart-contract-개발,-테스트_images1/50f9f05e.png)
   ![](.truffle,-ganache를-이용한-smart-contract-개발,-테스트_images1/d06f9c2d.png)
   ![](.truffle,-ganache를-이용한-smart-contract-개발,-테스트_images1/a1d925d6.png)
   ![](.truffle,-ganache를-이용한-smart-contract-개발,-테스트_images1/565d1185.png)

## References

1. [Honechain 따라하기_v0.9.pdf](https://sncsp.eagleoffice.co.kr/W/W00054/_layouts/15/WopiFrame2.aspx?sourcedoc=/W/W00054/D00001/개발%20가이드%20배포판/Honechain%20따라하기_v0.9.pdf&action=default&DefaultItemOpen=1)(박강훈 과장 작성,`비공개`) 기반으로 작성
